
#define _CRT_SECURE_NO_DEPRECATE // 1
#define _CRT_NONSTDC_NO_DEPRECATE // 1

#include "Removing_Labels_Fr_Color_Images_function.cpp"

int main()
{
	int doRemovingOfLabels_ColorImage(

		const Image& image_in,

		//const PARAMETERS_REMOVAL_OF_LABELS *sPARAMETERS_REMOVAL_OF_LABELSf,

		Image& image_out);

	int
		nRes;

	//const char inputFileName[] = "Orig_Red_Cyan.png";
	
		//const char inputFileName[] = "IM-0001-0738_CI.png";

		////////////////////////////////////////////////////////////////////////////////////////////////

	Image image_in;
	//image_in.read("Orig_Red_Cyan.png");
	
//	bool status = image_in.read("IM - 0001 - 0738_RF.png");
	//image_in.read("IM-0001-0738_CI.png");
	//image_in.read("01-019-0-LCC2_Mal_b_abL_c.jpg");
	//image_in.read("01-019-0-LMLO2_Mal_b_abL_c.jpg");

	//image_in.read("01-019-0-RCC_Mal_b_abL_c.jpg");
	//image_in.read("01-022-0-RCC_Mal_b_abR_c.jpg");
	//image_in.read("01-023-0-LMLO_Mal_b_abR_c.jpg");
	//image_in.read("01-023-0-RMLO_Mal_b_abR_c.jpg");
	//image_in.read("01-028-0-LCC_Mal_b_abL_c.jpg");
	//image_in.read("01-074-0-RMLO1_Mal_c_abL_c.jpg");
	//image_in.read("01-019-0-RCC_Mal_b_abL_c.jpg");
	//image_in.read("01-026-0-LCC_Mal_b_abR_c.jpg");

	//image_in.read("01-077-0-LCC_Mal_c_abL_c.jpg");
	//image_in.read("01-077-0-RCC_Mal_c_abL_c.jpg");
	//image_in.read("01-019-0-LCC1_Mal_b_abL_c.jpg");
///////////////////////////////////////////////////////////////////
	//image_in.read("03-079-0-LCC_Bio_b_abL_c.jpg");
	//image_in.read("03-079-0-LMLO_Bio_b_abL_c.jpg");
	//image_in.read("03-079-0-RCC_Bio_b_abL_c.jpg");
	//image_in.read("03-079-0-RMLO_Bio_b_abL_c.jpg");

	//image_in.read("01-022-0-LCC_Mal_b_abR_c.jpg");
	//image_in.read("01-019-A CC.jpg"); //??
	//image_in.read("03-017-0-RCC_Neg_b_abX_c.jpg");
	//image_in.read("03-027-0-LCC_Neg_c_abX_c.jpg");
	//image_in.read("03-032-0-RCC_Neg_a_abX_c.jpg"); 
	//image_in.read("03-037-0-LCC_Neg_d_abX_c.jpg");
	//image_in.read("03-037-0-RMLO_Neg_d_abX_c.jpg"); //
	//image_in.read("03-054-0-RCC_Rec_b_abR_c.jpg");

	//image_in.read("03-015-0-RCC_Neg_c_abX_c.jpg"); 
	//image_in.read("03-023-0-RCC_Neg_c_abX_c.jpg"); //
////////////////////////////////////////////////////////////////////
	//image_in.read("01-019-0-LCC3_Mal_b_abL_RF.jpg"); //
	//image_in.read("01-167-0-RMLO1_Neg_b_abX_c.jpg"); //
	//image_in.read("01-206-0-RMLO2_Neg_c_abX_c.jpg"); //
	//image_in.read("01-270-0-RCC2_Neg_d_abX_c.jpg"); //
	//image_in.read("01-270-0-RMLO2_Neg_d_abX_c.jpg"); //
	//image_in.read("01-290-0-RMLO_Bio_c_abR_c.jpg"); //
///////////////////////////////////////////////////////////////////////////////////////////
	//image_in.read("01-019-0-LCC1_Mal_b_abL_c.jpg"); //
	//image_in.read("01-019-0-LCC2_Mal_b_abL_c.jpg"); //
	//image_in.read("01-022-0-LMLO_Mal_b_abR_c.jpg");
	//image_in.read("01-201-0-RCC_Neg_c_abX_c.jpg");
	//image_in.read("01-220-0-LMLO2_Neg_c_abX_c.jpg");
	//image_in.read("03-037-0-RMLO_Neg_d_abX_c.jpg");
	image_in.read("01-022-0-LCC_Mal_b_abR_c.jpg");

	Image image_out;

	nRes = doRemovingOfLabels_ColorImage(
				//inputFileName, //const char *inputFileNamef,
				image_in, // Image& image_in,

		//&sPARAMETERS_REMOVAL_OF_LABELS, // const PARAMETERS_WEIGHTED_GRAY_3x3 *sPARAMETERS_REMOVAL_OF_LABELSf); // const float fWeight_Bluef)

		image_out); // Image& image_out);

	if (nRes == -1)
	{
		printf("\n\n The object side location has not been found.");
		printf("\n Please adjust the algorithm.");

		printf("\n\n Press any key to exit");	getchar(); 
		return 1;
	} // if (nRes == -1)

	//image_out.write("01-019-0-LCC2_Mal_b_abL_c_NoLabels.png");
	//image_out.write("01-019-0-LMLO2_Mal_b_abL_c_NoLabels.png");

	//image_out.write("01-019-0-RCC_Mal_b_abL_c_NoLabels.png");
	//image_out.write("01-022-0-RCC_Mal_b_abR_c_NoLabels.png");
	//image_out.write("01-023-0-LMLO_Mal_b_abR_c_NoLabels.png");
	//image_out.write("01-023-0-RMLO_Mal_b_abR_c_NoLabels.png");
	//image_out.write("01-028-0-LCC_Mal_b_abL_c_NoLabels.png");
	//image_out.write("01-074-0-RMLO1_Mal_c_abL_c_NoLabels.png");
	//image_out.write("01-019-0-RCC_Mal_b_abL_c_NoLabels.png");
	//image_out.write("01-026-0-LCC_Mal_b_abR_c_NoLabels.png");
	//image_out.write("01-077-0-LCC_Mal_c_abL_c_NoLabels.png");
	
	//image_out.write("01-019-0-LCC1_Mal_b_abL_c_NoLabels.png");
	//image_out.write("01-022-0-LCC_Mal_b_abR_c_NoLabels.png");
	//image_out.write("01-019-A CC_NoLabels.png");
	/////////////////////////////////////////////////////////
	//image_out.write("03-017-0-RCC_Neg_b_abX_c_NoLabels.png");
	//image_out.write("03-027-0-LCC_Neg_c_abX_c_NoLabels.png");
	//image_out.write("03-032-0-RCC_Neg_a_abX_c_NoLabels.png");
	//image_out.write("03-037-0-LCC_Neg_d_abX_c_NoLabels.png");
	//image_out.write("03-037-0-RMLO_Neg_d_abX_c_NoLabels.png"); 
	//image_out.write("03-054-0-RCC_Rec_b_abR_c_NoLabels.png");

	/////////////////////////////////////////////////////////////////////

	//image_out.write("03-079-0-LCC_Bio_b_abL_c_NoLabels.png");
	//image_out.write("03-079-0-LMLO_Bio_b_abL_c_NoLabels.png");
	//image_out.write("03-079-0-RCC_Bio_b_abL_c_NoLabels.png");
	//image_out.write("03-079-0-RMLO_Bio_b_abL_c_NoLabels.png");
	//image_out.write("03-015-0-RCC_Neg_c_abX_c_NoLabels.png"); 
	//image_out.write("03-023-0-RCC_Neg_c_abX_c_NoLabels.png");
///////////////////////////////////////////////////////////////////////
	//image_out.write("01-019-0-LCC3_Mal_b_abL_RF_NoLabels.png");
	//image_out.write("01-167-0-RMLO1_Neg_b_abX_c_NoLabels.png");
	//image_out.write("01-206-0-RMLO2_Neg_c_abX_c_NoLabels.png");
	//image_out.write("01-270-0-RCC2_Neg_d_abX_c_NoLabels.png");
	//image_out.write("01-270-0-RMLO2_Neg_d_abX_c_NoLabels.png");//
	//image_out.write("01-290-0-RMLO_Bio_c_abR_c_NoLabels.png");
////////////////////////////////////////////////////////////////////////////
	//image_out.write("01-019-0-LCC1_Mal_b_abL_c_NoLabels.png");
	//image_out.write("01-019-0-LCC2_Mal_b_abL_c_NoLabels.png");
	//image_out.write("01-022-0-LCC_Mal_b_abR_c_NoLabels.png");
	//image_out.write("01-022-0-LMLO_Mal_b_abR_c_NoLabels.png");
	//image_out.write("01-201-0-RCC_Neg_c_abX_c_NoLabels.png");
	//image_out.write("01-220-0-LMLO2_Neg_c_abX_c_NoLabels.png");
	//image_out.write("03-037-0-RMLO_Neg_d_abX_c_NoLabels.png");
	image_out.write("01-022-0-LCC_Mal_b_abR_c_NoLabels.png");
	
	printf("\n\n The image without labels has been saved");
	printf("\n\n Please press any key to exit");
	getchar(); //exit(1);
	return 1;
	  
} //int main()

//printf("\n\n Please press any key:"); getchar();