
#define _CRT_SECURE_NO_DEPRECATE // 1
#define _CRT_NONSTDC_NO_DEPRECATE // 1

#include "Removing_Labels_BatchMode_function.cpp"

static void list_directory(const char *dirname);

int main()
{
	int doRemovingOfLabels_ColorImage(

		const Image& image_in,

		Image& image_out);

	int
		nLenOfInputFileName,
		nNumOfProcessedFilesTot = 0,
		nNumOfCorrectlyProcessedFilesTot = 0,

		nRes;

	Image image_in;
	Image image_out;

	fout = fopen("wMain_RemovingLabels.txt", "w");

	if (fout == NULL)
	{
		printf("\n\n fout == NULL");
		getchar(); exit(1);
	} //if (fout == NULL)

/*
	image_in.read("C:\\Malignant_cases\\01-019-0-RCC_Mal_b_abL_c.jpg");

	nRes = doRemovingOfLabels_ColorImage(
		//inputFileName, //const char *inputFileNamef,
		image_in, // Image& image_in,

				  //&sPARAMETERS_REMOVAL_OF_LABELS, // const PARAMETERS_WEIGHTED_GRAY_3x3 *sPARAMETERS_REMOVAL_OF_LABELSf); // const float fWeight_Bluef)

		image_out); // Image& image_out);

	if (nRes == -1)
	{
		printf("\n\n The object side location has not been found.");
		printf("\n Please adjust the algorithm.");

		printf("\n\n Press any key to exit");	getchar();
		return 1;
	} // if (nRes == -1)

	  //image_out.write("01-019-0-LCC2_Mal_b_abL_c_NoLabels.png");
	  //image_out.write("01-019-0-LMLO2_Mal_b_abL_c_NoLabels.png");

	image_out.write("01-019-0-RCC_Mal_b_abL_c_NoLabels.png");

	printf("\n\n The image without labels has been saved");
	printf("\n\n Please press any key to exit");
	getchar(); exit(1);
*/

	//DIR *dir;
	//struct dirent *ent;
	DIR *pDIR;
	struct dirent *entry;


	char cFileName_inDirectory[100];

	char cInputFileName_Copy[100];
	//char cInput_Directory[100] = "C:\\Malignant_cases\\";
	//char cInput_Directory[100] = "C:\\03_Images\\";
	char cInput_Directory[100] = "C:\\Mammo_Images\\";
	

	char cOutputFileName[100];


#ifndef MASK_OUTPUT 
	char cNoLabelsPart_OfOutputFileName[100] = "_NoLabels.png";
#endif // #ifndef MASK_OUTPUT
	

#ifdef MASK_OUTPUT 
	char cNoLabelsPart_OfOutputFileName[100] = "_Mask.png";
#endif // #ifdef MASK_OUTPUT

	//if (pDIR = opendir("./data/item")) 
	//if (pDIR = opendir("C:\\Malignant_cases\\"))
	//if (pDIR = opendir("C:\\03_Images\\"))
	if (pDIR = opendir("C:\\Mammo_Images\\"))
	{
		while (entry = readdir(pDIR)) 
		{
			if (strcmp(entry->d_name, ".") != 0 && strcmp(entry->d_name, "..") != 0)
			{
				nNumOfProcessedFilesTot	+= 1;

				//cout << entry->d_name << "\n";
				printf("\n\n %s\n", entry->d_name);
				
				memset(cFileName_inDirectory, '\0', sizeof(cFileName_inDirectory));

				//strcpy(cFileName_inDirectory, "C:\\Malignant_cases\\");
				strcpy(cFileName_inDirectory, cInput_Directory);

				strcat(cFileName_inDirectory, entry->d_name);
				
				printf("\n Concatenated input file: %s\n", cFileName_inDirectory);

				fprintf(fout,"\n\n Concatenated input file: %s", cFileName_inDirectory);

				//image_in.read(cInput_Directory);
				image_in.read(cFileName_inDirectory);

				printf("\n\n After reading the input image");

				nRes = doRemovingOfLabels_ColorImage(
					//inputFileName, //const char *inputFileNamef,
					image_in, // Image& image_in,

							  //&sPARAMETERS_REMOVAL_OF_LABELS, // const PARAMETERS_WEIGHTED_GRAY_3x3 *sPARAMETERS_REMOVAL_OF_LABELSf); // const float fWeight_Bluef)

					image_out); // Image& image_out);

				if (nRes == -1)
				{
					fprintf(fout,"\n\n The labels have not been removed from the image %s", cFileName_inDirectory);
					printf("\n\n The labels have not been removed from the image %s", cFileName_inDirectory);
										
				} // if (nRes == -1)
				else if (nRes == 1)
				{
					nNumOfCorrectlyProcessedFilesTot += 1;
					memset(cOutputFileName, '\0', sizeof(cOutputFileName));
					memset(cInputFileName_Copy, '\0', sizeof(cInputFileName_Copy));

					strcpy(cInputFileName_Copy, entry->d_name);

					nLenOfInputFileName = strlen(cInputFileName_Copy);
					cInputFileName_Copy[nLenOfInputFileName - 4] = '\0';

					//strcpy(cOutputFileName, entry->d_name);
					strcpy(cOutputFileName, cInputFileName_Copy);

					strcat(cOutputFileName, cNoLabelsPart_OfOutputFileName);

					//image_out.write("Image_NoLabels.png");
					image_out.write(cOutputFileName);

					fprintf(fout, "\n\n %s image has been saved", cOutputFileName);

					printf("\n\n The image '%s'without labels has been saved", cOutputFileName);

					printf("\n\n nNumOfCorrectlyProcessedFilesTot = %d, nNumOfProcessedFilesTot = %d", nNumOfCorrectlyProcessedFilesTot, nNumOfProcessedFilesTot);

					fprintf(fout,"\n\n nNumOfCorrectlyProcessedFilesTot = %d, nNumOfProcessedFilesTot = %d", nNumOfCorrectlyProcessedFilesTot, nNumOfProcessedFilesTot);

					//printf("\n\n Press any key to go to the next image");	getchar();
				} //else if (nRes == 1)

				//printf("\n\n Press any key to go to the next image");	getchar();
				fflush(fout);
			}//if (strcmp(entry->d_name, ".") != 0 && strcmp(entry->d_name, "..") != 0)
		}//while (entry = readdir(pDIR)) 

		closedir(pDIR);
	}//if (pDIR = opendir("C:\\03_Images\\") //C:\\Malignant_cases\\"))
	else 
	{
		printf("\n The directory can not be opened");
		printf("\n\n Press any key to exit");	getchar();
		// could not open directory 
		perror("");
		return EXIT_FAILURE;
	}//else


	/*
	char cInput_Directory[100] = "C:\\Malignant_cases\\";
	char cFileName_inDirectory[100];

	//if (pDIR = opendir("./data/item")) 
	if (pDIR = opendir("C:\\Malignant_cases\\"))
	{
		while (entry = readdir(pDIR))
		{
			if (strcmp(entry->d_name, ".") != 0 && strcmp(entry->d_name, "..") != 0)
			{
				//cout << entry->d_name << "\n";
				printf("%s\n", entry->d_name);

				//strcat(cInput_Directory, entry->d_name);
				//strcpy(cFileName_inDirectory, cInput_Directory);

				strcpy(cFileName_inDirectory, cInput_Directory);

				strcat(cFileName_inDirectory, entry->d_name);

				printf("\n Concatenated input file: %s\n", cFileName_inDirectory);

				fprintf(fout, "\n\n Concatenated input file: %s", cFileName_inDirectory);
				fflush(fout);

				printf("\n\n Press any key to continue");	getchar();

				//image_in.read(cInput_Directory);
				image_in.read(cFileName_inDirectory);

				nRes = doRemovingOfLabels_ColorImage(
					//inputFileName, //const char *inputFileNamef,
					image_in, // Image& image_in,

							  //&sPARAMETERS_REMOVAL_OF_LABELS, // const PARAMETERS_WEIGHTED_GRAY_3x3 *sPARAMETERS_REMOVAL_OF_LABELSf); // const float fWeight_Bluef)

					image_out); // Image& image_out);

				if (nRes == -1)
				{
					printf("\n\n The object side location has not been found.");
					printf("\n Please adjust the algorithm.");

					printf("\n\n Press any key to exit");	getchar();
					return 1;
				} // if (nRes == -1)

				image_out.write("Image_NoLabels.png");

				printf("\n\n The image without labels has been saved");
				//printf("\n\n After reading an image from a directory");	
				getchar();
			}//if (strcmp(entry->d_name, ".") != 0 && strcmp(entry->d_name, "..") != 0)
		}//while (entry = readdir(pDIR)) 
		closedir(pDIR);
	}//if (pDIR = opendir("C:\\Malignant_cases\\"))
	else
	{
		printf("\n The directory can not be opened");
		printf("\n\n Press any key to exit");	getchar();
		// could not open directory 
		perror("");
		return EXIT_FAILURE;
	}//else
	////////////////////////////////////////////////////////////////////////////////////////////////
*/


	printf("\n\n Press any key to exit");	getchar();
	return 1;
	  
} //int main()

static void list_directory(
	const char *dirname)
{
	struct dirent **files;
	int i;
	int n;

	// Scan files in directory
	n = scandir(dirname, &files, NULL, alphasort);
	if (n >= 0) {

		// Loop through file names 
		for (i = 0; i < n; i++) {
			struct dirent *ent;

			// Get pointer to file entry 
			ent = files[i];

			// Output file name
			switch (ent->d_type) {
			case DT_REG:
				printf("%s\n", ent->d_name);
				break;

			case DT_DIR:
				printf("%s/\n", ent->d_name);
				break;

			case DT_LNK:
				printf("%s@\n", ent->d_name);
				break;

			default:
				printf("%s*\n", ent->d_name);
			}

		}

		// Release file names
		for (i = 0; i < n; i++) {
			free(files[i]);
		}
		free(files);

	}
	else {
		printf("Cannot open directory %s\n", dirname);
	}
} // static void list_directory(...

/*
static void list_directory(const char *dirname);

//int main(int argc, char *argv[])
int main()
{

DIR *dir;
struct dirent *ent;

//if ((dir = opendir("c:\\src\\")) != NULL) 

if ((dir = opendir("C:\\Malignant_cases\\")) != NULL)
{
	// print all the files and directories within directory
	while ((ent = readdir(dir)) != NULL) {
		printf("%s\n", ent->d_name);
	}
	closedir(dir);
}
else {
	// could not open directory 
	perror("");
	return EXIT_FAILURE;
}

printf("\n\nPlease press any key:"); getchar();
return 1;
}//int main()

 
static void list_directory(
	const char *dirname)
{
	struct dirent **files;
	int i;
	int n;

	// Scan files in directory
	n = scandir(dirname, &files, NULL, alphasort);

	if (n >= 0) {

		// Loop through file names 
		for (i = 0; i < n; i++) {
			struct dirent *ent;

			// Get pointer to file entry 
			ent = files[i];

			// Output file name
			switch (ent->d_type) {
			case DT_REG:
				printf("%s\n", ent->d_name);
				break;

			case DT_DIR:
				printf("%s/\n", ent->d_name);
				break;

			case DT_LNK:
				printf("%s@\n", ent->d_name);
				break;

			default:
				printf("%s*\n", ent->d_name);
			}

		}

		// Release file names
		for (i = 0; i < n; i++) {
			free(files[i]);
		}
		free(files);

	}
	else {
		printf("Cannot open directory %s\n", dirname);
	}
}
*/

/*
#include<stdio.h>
#include<cstdlib>
#include<iostream>
#include<string.h>
#include<fstream>
#include<dirent.h>

void listFile();

int main(){
listFile();
return 0;
}

void listFile(){
	DIR *pDIR;
	struct dirent *entry;
	if( pDIR=opendir("./data/item") ){
	while(entry = readdir(pDIR)){
	if( strcmp(entry->d_name, ".") != 0 && strcmp(entry->d_name, "..") != 0 )
	cout << entry->d_name << "\n";
	}
	closedir(pDIR);
}

}
*/
//printf("\n\n Please press any key:"); getchar();