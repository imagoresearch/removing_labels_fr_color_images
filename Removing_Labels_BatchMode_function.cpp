

#include "Removing_Labels_BatchMode_parameters.h"

#include "Removing_Labels_BatchMode_2.h"

using namespace imago;

FILE *fout;

int doRemovingOfLabels_ColorImage(
	const Image& image_in,

	//const PARAMETERS_REMOVAL_OF_LABELS *sParameters_Removal_Of_Labelsf,

	Image& image_out)
{

	int Initializing_Color_To_CurSize(
		const int nImageWidthf,
		const int nImageLengthf,

		COLOR_IMAGE *sColor_Imagef);

	int DeterminingSideOfObjectLocation_AndBackground(
		//const PARAMETERS_REMOVAL_OF_LABELS *sParameters_Removal_Of_Labelsf,

		COLOR_IMAGE *sColor_Imagef); 

	int DeterminingLenOfObjectBoundary(

		COLOR_IMAGE *sColor_Imagef);

	int Erasing_Labels(

		COLOR_IMAGE *sColor_Imagef); 

	int Detecting_BackgroundPixels(

		COLOR_IMAGE *sColor_Imagef);

	int FillingOut_BlackObjectPixels(

		COLOR_IMAGE *sColor_Imagef);

	int
		nRes;

///////////////////////////////////////////////////////////////////////////////
/*
	fout = fopen("wMain_RemovingLabels.txt", "w");

	if (fout == NULL)
	{
		printf("\n\n fout == NULL");
		getchar(); exit(1);
	} //if (fout == NULL)
*/
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	int
		i,
		j,

		nSizeOfImage, // = nImageWidth*nImageHeight,

		nIndexOfPixelCur,

		iLen,
		iWid,

		nRed,
		nGreen,
		nBlue,

		nIntensity_Read_Test_ImageMax = -nLarge,

		nImageWidth,
		nImageHeight;

	////////////////////////////////////////////////////////////////////////

	// size of image
		nImageWidth = image_in.width();

		nImageWidth = image_in.width();
		nImageHeight = image_in.height();

	printf("\n\n nImageWidth = %d, nImageHeight = %d", nImageWidth, nImageHeight);

	fprintf(fout, "\n\n nImageWidth = %d, nImageHeight = %d", nImageWidth, nImageHeight);

	if (nImageWidth > nLenMax || nImageWidth < nLenMin)
	{
		fprintf(fout,"\n\n An error in reading the image width: nImageWidth = %d", nImageWidth);

		printf("\n\n An error in reading the image width: nImageWidth = %d", nImageWidth);
		return (-1);
		//printf("\n\n Please press any key to exit");
		//fflush(fout);
		getchar(); exit(1);

	} // if (nImageWidth > nLenMax || nImageWidth < nLenMin)

	if (nImageHeight > nWidMax || nImageHeight < nWidMin)
	{
		printf("\n\n An error in reading the image height: nImageHeight = %d", nImageHeight);
		return (-1);
		//printf("\n\n Please press any key to exit");
		//getchar(); exit(1);

	} // if (nImageHeight > nWidMax || nImageHeight < nWidMin)

	COLOR_IMAGE sColor_Image; //

	nRes = Initializing_Color_To_CurSize(
		nImageHeight, //const int nImageWidthf,
		nImageWidth, //const int nImageLengthf,

		&sColor_Image); // COLOR_IMAGE *sColor_Imagef);

	nSizeOfImage = nImageWidth*nImageHeight;

//finding 'nIntensity_Read_Test_ImageMax' to decide if the intensity rescaling to (0,255) is needed
	for (j = 0; j < nImageHeight; j++)
	{
		for (i = 0; i < nImageWidth; i++)
		{
			nIndexOfPixelCur = i + j*nImageWidth;

			nRed = image_in(j, i, R);
			nGreen = image_in(j, i, G);
			nBlue = image_in(j, i, B);

			if (nRed < 0 || nRed > nIntensityStatForReadMax || nGreen < 0 || nGreen > nIntensityStatForReadMax || nBlue < 0 || nBlue > nIntensityStatForReadMax)
			{
				fprintf(fout,"\n\n An error in reading the image: nRed = %d, nGreen = %d, nBlue = %d, i = %d, j = %d", nRed, nGreen, nBlue, i, j);
				printf("\n\n An error in reading the image: nRed = %d, nGreen = %d, nBlue = %d, i = %d, j = %d", nRed, nGreen, nBlue, i, j);
				
				return (-1);
				//printf("\n\n Please press any key to exit");
				//getchar(); exit(1);
			} // if (nRed < 0 || nRed > nIntensityStatForReadMax || nGreen < 0 || nGreen > nIntensityStatForReadMax || nBlue < 0 || nBlue > nIntensityStatForReadMax)

			if (nRed > nIntensity_Read_Test_ImageMax)
				nIntensity_Read_Test_ImageMax = nRed;

				if (nGreen > nIntensity_Read_Test_ImageMax)
				nIntensity_Read_Test_ImageMax = nGreen;

				if (nBlue > nIntensity_Read_Test_ImageMax)
				nIntensity_Read_Test_ImageMax = nBlue;

			//fprintf(fout, "\n nPixelArr[%d] = %d, i = %d, j = %d", nIndexOfPixel, nPixelArr[nIndexOfPixel], i, j);
		} // for (int i = 0; i < nImageWidth; i++)

	}//for (int j = 0; j < nImageHeight; j++)

	printf("\n\n nIntensity_Read_Test_ImageMax = %d", nIntensity_Read_Test_ImageMax);
	fprintf(fout, "\n\n nIntensity_Read_Test_ImageMax = %d", nIntensity_Read_Test_ImageMax);
////////////////////////////////////////////////////////////////////////////////////////////

	for (j = 0; j < nImageHeight; j++)
	{
		for (i = 0; i < nImageWidth; i++)
		{
			nIndexOfPixelCur = i + j*nImageWidth;

			nRed = image_in(j, i, R);
			nGreen = image_in(j, i, G);
			nBlue = image_in(j, i, B);

			iLen = i;
			iWid = j;

			if (nIntensity_Read_Test_ImageMax > nIntensityStatMax)
			{
//rescaling

			sColor_Image.nRed_Arr[nIndexOfPixelCur] = nRed  / 256;

			sColor_Image.nGreen_Arr[nIndexOfPixelCur] = nGreen / 256;
			sColor_Image.nBlue_Arr[nIndexOfPixelCur] = nBlue  / 256;

			//////////////////////////////////////////////////////////////////////////////////////////////////////
			sColor_Image.nRed_Arr[nIndexOfPixelCur] = nRed;
			sColor_Image.nGreen_Arr[nIndexOfPixelCur] = nGreen;
			sColor_Image.nBlue_Arr[nIndexOfPixelCur] = nBlue;

			} //if (nIntensity_Read_Test_ImageMax > nIntensityStatMax)
			else
			{
// no rescaling

			sColor_Image.nRed_Arr[nIndexOfPixelCur] = nRed;
			sColor_Image.nGreen_Arr[nIndexOfPixelCur] = nGreen;
			sColor_Image.nBlue_Arr[nIndexOfPixelCur] = nBlue;
			} //else

			//fprintf(fout, "\n nPixelArr[%d] = %d, i = %d, j = %d", nIndexOfPixel, nPixelArr[nIndexOfPixel], i, j);
		} // for (int i = 0; i < nImageWidth; i++)
	}//for (int j = 0; j < nImageHeight; j++)

	//printf("\n\n 1: please press any key"); getchar();
//#ifdef INCLUDE_RGB_FROM_WIGHTED_HLS_BEFORE_PROCESSING

	//printf("\n\n 2: please press any key"); getchar();

///////////////////////////////////////////////////////////////////////////
	
	nRes = DeterminingSideOfObjectLocation_AndBackground(

		&sColor_Image); //COLOR_IMAGE *sColor_Imagef, //[nImageSizeMax]

	if (nRes == -1)
	{
		delete[] sColor_Image.nRed_Arr;
		delete[] sColor_Image.nGreen_Arr;
		delete[] sColor_Image.nBlue_Arr;

		delete[] sColor_Image.nLenObjectBoundary_Arr;

		return -1;
	}// if (nRes == -1)

	nRes = DeterminingLenOfObjectBoundary(

		&sColor_Image); //COLOR_IMAGE *sColor_Imagef) //[nImageSizeMax]

	if (nRes == -1)
	{
		delete[] sColor_Image.nRed_Arr;
		delete[] sColor_Image.nGreen_Arr;
		delete[] sColor_Image.nBlue_Arr;

		delete[] sColor_Image.nLenObjectBoundary_Arr;

		return -1;
	}// if (nRes == -1)

	nRes = Erasing_Labels(

		&sColor_Image); //COLOR_IMAGE *sColor_Imagef);
/////////////////////////////////////////////////////////////////////////////////////////////

	nRes = Detecting_BackgroundPixels(

		&sColor_Image); //COLOR_IMAGE *sColor_Imagef)
/////////////////////////////////////////////////////////////////////////////////////////////

	nRes = FillingOut_BlackObjectPixels(

		&sColor_Image); //COLOR_IMAGE *sColor_Imagef)

/////////////////////////////////////////////////////////////////////////////////////////////


	int bytesOfWidth = image_in.pitchInBytes();

	int nStep = image_in.pitchInBytes() / (image_in.width() * sizeof(unsigned char));
	Image imageToSave(nImageWidth, nImageHeight, bytesOfWidth);

#ifndef MASK_OUTPUT 
	  // Save to file

	for (j = 0; j < imageToSave.height(); j++)
	{
		for (i = 0; i < imageToSave.width(); i++)
		{
			iLen = i;
			iWid = j;

			nIndexOfPixelCur = iLen + iWid*nImageWidth;
	
			nRed = sColor_Image.nRed_Arr[nIndexOfPixelCur];
			nGreen = sColor_Image.nGreen_Arr[nIndexOfPixelCur];
			nBlue = sColor_Image.nBlue_Arr[nIndexOfPixelCur];

			imageToSave(j, i, R) = nRed;
			imageToSave(j, i, G) = nGreen;
			imageToSave(j, i, B) = nBlue;

			imageToSave(j, i, A) = image_in(j, i, A);
			//image_out(j, i, A) = image_in(j, i, A);

		} // for (i = 0; i < imageToSave.width(); i++)

	} // for (j = 0; j < imageToSave.height(); j++)
#endif // #ifndef MASK_OUTPUT

#ifdef MASK_OUTPUT 
	  // Save to file

	for (j = 0; j < imageToSave.height(); j++)
	{
		for (i = 0; i < imageToSave.width(); i++)
		{
			iLen = i;
			iWid = j;

			nIndexOfPixelCur = iLen + iWid*nImageWidth;

			nRed = sColor_Image.nRed_Arr[nIndexOfPixelCur];
			nGreen = sColor_Image.nGreen_Arr[nIndexOfPixelCur];
			nBlue = sColor_Image.nBlue_Arr[nIndexOfPixelCur];

	//		if (nRed >= nIntensityCloseToBlackMax || nGreen >= nIntensityCloseToBlackMax ||
		//		nBlue >= nIntensityCloseToBlackMax)
			if (nRed > sColor_Image.nIntensityOfBackground_Red || nGreen > sColor_Image.nIntensityOfBackground_Green ||
				nBlue > sColor_Image.nIntensityOfBackground_Blue)
			{

				imageToSave(j, i, R) = nIntensityStatMax; // nRed;
				imageToSave(j, i, G) = nIntensityStatMax, //nGreen;
					imageToSave(j, i, B) = nIntensityStatMax; // nBlue;
			} //if (nRed > sColor_Image.nIntensityOfBackground_Red || nGreen > sColor_Image.nIntensityOfBackground_Green ||
			else
			{
				imageToSave(j, i, R) = sColor_Image.nIntensityOfBackground_Red;
				imageToSave(j, i, G) = sColor_Image.nIntensityOfBackground_Green;
				imageToSave(j, i, B) = sColor_Image.nIntensityOfBackground_Blue;

			}//else

			imageToSave(j, i, A) = image_in(j, i, A);
			//image_out(j, i, A) = image_in(j, i, A);

		} // for (i = 0; i < imageToSave.width(); i++)

	} // for (j = 0; j < imageToSave.height(); j++)

#endif // #ifdef MASK_OUTPUT
////////////////////////////////////////////////////////////////////////////
	//imageToSave.write("image_NoLabels.png");
	//imageToSave.write(outputFileNamef);
	
	image_out = imageToSave;

	delete [] sColor_Image.nRed_Arr;
	delete[] sColor_Image.nGreen_Arr;
	delete[] sColor_Image.nBlue_Arr;

	delete[] sColor_Image.nLenObjectBoundary_Arr;
//	printf("\n\n The grayscale image has been saved");
	//printf("\n\n Please press any key to exit");	getchar(); 

	return 1;
} //int doRemovingOfLabels_ColorImage(...
///////////////////////////////////////////////////////////////////////
  
int DeterminingSideOfObjectLocation_AndBackground(
	COLOR_IMAGE *sColor_Imagef) 
	
{
	int
		nResf,
		nIndexOfPixelCurf,
		nWidCurf = (int)( (float)(sColor_Imagef->nWidth)*fShareOfWidthForTheSamePixelIntensities_1 ),
		iLenf,

		nRedCurf,
		nGreenCurf,
		nBlueCurf,

		nRedPrevf,
		nGreenPrevf,
		nBluePrevf,

		nLengthOfTheSamePixelIntensitiesf;
	
	//left, initially
	nLengthOfTheSamePixelIntensitiesf = 0;

		//for (iWidf = 0; iWidf < sColor_Imagef->nWidth; iWidf++)
	fprintf(fout, "\n\n 'DeterminingSideOfObjectLocation_AndBackground' - left\n");

		for (iLenf = 0; iLenf < sColor_Imagef->nLength; iLenf++)
		{
			nIndexOfPixelCurf = iLenf + (nWidCurf*sColor_Imagef->nLength); // nLenMax);

			nRedCurf = sColor_Imagef->nRed_Arr[nIndexOfPixelCurf];
			nGreenCurf = sColor_Imagef->nGreen_Arr[nIndexOfPixelCurf];
			nBlueCurf = sColor_Imagef->nBlue_Arr[nIndexOfPixelCurf];
	
			if (iLenf == 0)
			{
			nRedPrevf = nRedCurf;
			nGreenPrevf = nGreenCurf;
			nBluePrevf = nBlueCurf;

			}  //if (iLenf == 0)

			if (nRedCurf != nRedPrevf || nGreenCurf != nGreenPrevf || nBlueCurf != nBluePrevf)
			{
				fprintf(fout, "\n\n No object to the left: iLenf = %d, nRedCurf = %d, nGreenCurf = %d, nBlueCurf = %d", iLenf, nRedCurf, nGreenCurf, nBlueCurf);
				fprintf(fout, "\n nRedPrevf = %d, nGreenPrevf = %d, nBluePrevf = %d", nRedPrevf, nGreenPrevf, nBluePrevf);

				break;
			} // if (nRedCurf != nRedPrevf || nGreenCurf != nGreenPrevf || nBlueCurf != nBluePrevf)

			nLengthOfTheSamePixelIntensitiesf += 1;

			fprintf(fout, "\n Left: iLenf = %d, nLengthOfTheSamePixelIntensitiesf = %d, nRedCurf = %d, nGreenCurf = %d, nBlueCurf = %d", 
				iLenf, nLengthOfTheSamePixelIntensitiesf, nRedCurf, nGreenCurf, nBlueCurf);

			if (nLengthOfTheSamePixelIntensitiesf == nLengthOfTheSamePixelIntensitiesMin)
			{
				sColor_Imagef->nSideOfObjectLocation = 1; // object to the right
				sColor_Imagef->nIntensityOfBackground_Red = nRedCurf;
				sColor_Imagef->nIntensityOfBackground_Green = nGreenCurf;
				sColor_Imagef->nIntensityOfBackground_Blue = nBlueCurf;

				fprintf(fout, "\n\n Object to the right: Color_Imagef->nSideOfObjectLocation = 1, sColor_Imagef->nIntensityOfBackground_Red = %d, ..._Green = %d, ....Blue = %d",
					sColor_Imagef->nIntensityOfBackground_Red, sColor_Imagef->nIntensityOfBackground_Green, sColor_Imagef->nIntensityOfBackground_Blue);

				return 1;
			} // if (nLengthOfTheSamePixelIntensitiesf == nLengthOfTheSamePixelIntensitiesMin)

		} //for (iLenf = 0; iLenf <  sColor_Imagef->nLength; iLenf++)
	
//right
		nLengthOfTheSamePixelIntensitiesf = 0;

		fprintf(fout, "\n\n 'DeterminingSideOfObjectLocation_AndBackground' - right\n");
		//for (iWidf = 0; iWidf < sColor_Imagef->nWidth; iWidf++)

		for (iLenf = sColor_Imagef->nLength - 1; iLenf >= 0; iLenf--)
		{
			nIndexOfPixelCurf = iLenf + (nWidCurf*sColor_Imagef->nLength); // nLenMax);

			nRedCurf = sColor_Imagef->nRed_Arr[nIndexOfPixelCurf];
			nGreenCurf = sColor_Imagef->nGreen_Arr[nIndexOfPixelCurf];
			nBlueCurf = sColor_Imagef->nBlue_Arr[nIndexOfPixelCurf];

			if (iLenf == sColor_Imagef->nLength - 1)
			{
				nRedPrevf = nRedCurf;
				nGreenPrevf = nGreenCurf;
				nBluePrevf = nBlueCurf;

			}  //if (iLenf == sColor_Imagef->nLength - 1)

			if (nRedCurf != nRedPrevf || nGreenCurf != nGreenPrevf || nBlueCurf != nBluePrevf)
			{

				fprintf(fout, "\n\n No object to the right: iLenf = %d, nRedCurf = %d, nGreenCurf = %d, nBlueCurf = %d", iLenf, nRedCurf, nGreenCurf, nBlueCurf);
				fprintf(fout, "\n nRedPrevf = %d, nGreenPrevf = %d, nBluePrevf = %d", nRedPrevf, nGreenPrevf, nBluePrevf);

				return -1; // no left or right object location
			} // if (nRedCurf != nRedPrevf || nGreenCurf != nGreenPrevf || nBlueCurf != nBluePrevf)

			nLengthOfTheSamePixelIntensitiesf += 1;

			fprintf(fout, "\n Right: iLenf = %d, nLengthOfTheSamePixelIntensitiesf = %d, nRedCurf = %d, nGreenCurf = %d, nBlueCurf = %d",
				iLenf, nLengthOfTheSamePixelIntensitiesf, nRedCurf, nGreenCurf, nBlueCurf);

			if (nLengthOfTheSamePixelIntensitiesf == nLengthOfTheSamePixelIntensitiesMin)
			{
				sColor_Imagef->nSideOfObjectLocation = -1; // // object to the left
				sColor_Imagef->nIntensityOfBackground_Red = nRedCurf;
				sColor_Imagef->nIntensityOfBackground_Green = nGreenCurf;
				sColor_Imagef->nIntensityOfBackground_Blue = nBlueCurf;

				fprintf(fout, "\n\n Object to the left: Color_Imagef->nSideOfObjectLocation = -1, sColor_Imagef->nIntensityOfBackground_Red = %d, ..._Green = %d, ....Blue = %d",
					sColor_Imagef->nIntensityOfBackground_Red, sColor_Imagef->nIntensityOfBackground_Green, sColor_Imagef->nIntensityOfBackground_Blue);

				return 1;
			} // if (nLengthOfTheSamePixelIntensitiesf == nLengthOfTheSamePixelIntensitiesMin)

		} //for (iLenf = sColor_Imagef->nLength; iLenf >= 0; iLenf--)

		printf("\n\n An error in 'DeterminingSideOfObjectLocation_AndBackground': no left or right object location");
		fprintf(fout,"\n\n An error in 'DeterminingSideOfObjectLocation_AndBackground': no left or right object location");

		return (-1);
		//printf("\n\n Please press any key to exit");
		//getchar(); exit(1);

	return 1;
} //int DeterminingSideOfObjectLocation_AndBackground(...
////////////////////////////////////////////////////////////////////////////////

int DeterminingLenOfObjectBoundary(

	COLOR_IMAGE *sColor_Imagef)
{
	int CharacteristicsOfNeighborsInARow(

		const COLOR_IMAGE *sColor_Imagef, //[nImageSizeMax]

		const int nWidCurf,
		const int nLenCurf,
		/////////////////////////////////////////////////////////////
		int &nNumOfBlackNeighboingPixelsFrTheSameRowf,
		int &nAverIntensityOverNeighboingPixelsFrTheSameRowf,
		int &nDiffBetweenMaxAndMinf);

	int
		nResf,

		nNumOfBlackNeighboingPixelsFrTheSameRowf,
		nAverIntensityOverNeighboingPixelsFrTheSameRowf,
		nDiffBetweenMaxAndMinf,
		nIs_ExitFromALinePossiblef = 0, //not possible initially

		nIndexOfPixelCurf,
		nIndexOfPixelBelowf,
		nIndexOfPixelSidef,

		//nWidCurf = (int)(sColor_Imagef->nWidth*fShareOfWidthForTheSamePixelIntensities_1),
		iWidf,
		iLenf,
		nLenSidef,

		nRedCurf,
		nGreenCurf,
		nBlueCurf,

		nRedPrevf = 0,
		nNumOfNonZeroBeginningsInARowf = 0,
		nNumOfNonZeroEndsInARowf = 0,

		nRedBelowf,
		nGreenBelowf,
		nBlueBelowf,

		nRedSidef,
		nGreenSidef,
		nBlueSidef,

		nNumOfRightBoundariesf,
		nNumOfLeftBoundariesf,

		nLenOfObjectBoundaryCurf,

		nLenOf_1stObjectBoundaryCurf,
		nLenOf_2ndObjectBoundaryCurf,

		nIsObjectPixelBelowf = 0, // no

		nNumOfAddedWidf = 0,

		nWidBelowf,
		nDiffOfLens_Maxf = nDistBetweenBoundariesOfNeighborWids_Max + nLengthOfTheSamePixelIntensities_ForObjectBoundary_Min,
		nDistFromLastBoundaryTo_2ndOnef,

		nDistOf1stObjectPixel_ToImageEdgeMaxf = (int)(fCoefDistOf1stObjectPixel_ToImageEdgeMax*(float)(sColor_Imagef->nLength)),
		nNumOfNonZeroObjectBoundariesf = 0,
		nWidForfNonZeroObjectBoundariesf = nLarge,

		nDiffOfLensf,

		nDiffOfLensForBoundariesf,

		nLengthOfTheSamePixelIntensities_ForObjectBoundary_Curf;

	fprintf(fout, "\n\n //////////////////////////////////////////////////////////////////////////////");
	fprintf(fout, "\n\n 'DeterminingLenOfObjectBoundary': sColor_Imagef->nSideOfObjectLocation = %d, sColor_Imagef->nIntensityOfBackground_Red = %d, ..._Green = %d, ....Blue = %d",
		sColor_Imagef->nSideOfObjectLocation, sColor_Imagef->nIntensityOfBackground_Red, sColor_Imagef->nIntensityOfBackground_Green, sColor_Imagef->nIntensityOfBackground_Blue);

	for (iWidf = 0; iWidf < sColor_Imagef->nWidth; iWidf++)
	{

		if (iWidf >= nWidSubimageToPrintMin && iWidf <= nWidSubimageToPrintMax)
		{
			fprintf(fout, "\n\n /////////////////////////////////////////////////////////////////////////");

			fprintf(fout, "\n Next iWidf = %d within the range: ", iWidf);

			nNumOfNonZeroBeginningsInARowf = 0;
			nNumOfNonZeroEndsInARowf = 0;

			nRedPrevf = 0;
		}  //if (iWidf >= nWidSubimageToPrintMin && iWidf <= nWidSubimageToPrintMax)

		for (iLenf = 0; iLenf < sColor_Imagef->nLength; iLenf++)
		{
			if (iWidf >= nWidSubimageToPrintMin && iWidf <= nWidSubimageToPrintMax && iLenf >= nLenSubimageToPrintMin && iLenf <= nLenSubimageToPrintMax)
			{
				nIndexOfPixelCurf = iLenf + (iWidf*sColor_Imagef->nLength); // nLenMax);

				nRedCurf = sColor_Imagef->nRed_Arr[nIndexOfPixelCurf];
				nGreenCurf = sColor_Imagef->nGreen_Arr[nIndexOfPixelCurf];
				nBlueCurf = sColor_Imagef->nBlue_Arr[nIndexOfPixelCurf];

				if (nRedCurf != sColor_Imagef->nIntensityOfBackground_Red && nRedPrevf == sColor_Imagef->nIntensityOfBackground_Red)
				{
					nNumOfNonZeroBeginningsInARowf += 1;
					fprintf(fout, "\n A next nNumOfNonZeroBeginningsInARowf = %d, iWidf = %d ", nNumOfNonZeroBeginningsInARowf, iWidf);
				} //if (nRedCurf != sColor_Imagef->nIntensityOfBackground_Red && nRedPrevf == sColor_Imagef->nIntensityOfBackground_Red)

				if (nRedCurf == sColor_Imagef->nIntensityOfBackground_Red && nRedPrevf != sColor_Imagef->nIntensityOfBackground_Red)
				{
					nNumOfNonZeroEndsInARowf += 1;
					fprintf(fout, "\n      A next nNumOfNonZeroEndsInARowf = %d, iWidf = %d ", nNumOfNonZeroEndsInARowf, iWidf);
				} //if (nRedCurf == sColor_Imagef->nIntensityOfBackground_Red && nRedPrevf != sColor_Imagef->nIntensityOfBackground_Red)

				fprintf(fout, "%d:%d,", iLenf, nRedCurf);
				nRedPrevf = nRedCurf;

				if (iLenf == sColor_Imagef->nLength - 1)
				{
					fprintf(fout, "\n The final nNumOfNonZeroBeginningsInARowf = %d, nNumOfNonZeroEndsInARowf = %d for iWidf = %d ", nNumOfNonZeroBeginningsInARowf, nNumOfNonZeroEndsInARowf, iWidf);
				}//if (iLenf == sColor_Imagef->nLength - 1)
			} //if (iWidf >= nWidSubimageToPrintMin && iWidf <= nWidSubimageToPrintMax && iLenf >= nLenSubimageToPrintMin && iLenf <= nLenSubimageToPrintMax)

		} //for (iLenf = 0; iLenf < sColor_Imagef->nLength; iLenf++)

	} // for (iWidf = 0; iWidf < sColor_Imagef->nWidth; iWidf++)

	fflush(fout);
	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//object to the left  
	if (sColor_Imagef->nSideOfObjectLocation == -1)
	{
		//fflush(fout);
		fprintf(fout, "\n\n The object is to the left");

		nWidForfNonZeroObjectBoundariesf = -1; // initially
		for (iWidf = 0; iWidf < sColor_Imagef->nWidth; iWidf++)
		{
			if (iWidf > 0 && sColor_Imagef->nLenObjectBoundary_Arr[iWidf - 1] == -1)
			{
				fprintf(fout, "\n\n An error: the object boundary has not been found at iWidf - 1 = %d", iWidf - 1);
				printf("\n\n An error: the object boundary has not been found at iWidf - 1 = %d", iWidf - 1);

				return (-1);
				//printf("\n\n Please adjust the settings and press any key to exit");
				//getchar(); exit(1);
			} //if (iWidf > 0 && sColor_Imagef->nLenObjectBoundary_Arr[iWidf - 1] == -1)

			nNumOfRightBoundariesf = 0;
			nLengthOfTheSamePixelIntensities_ForObjectBoundary_Curf = 0;

			nIs_ExitFromALinePossiblef = 0; //initially
			nLenOf_1stObjectBoundaryCurf = -1;
			nLenOf_2ndObjectBoundaryCurf = -1;

			//nDiffOfLensf = -nLarge;

			fprintf(fout, "\n\n The length boundaries for a left object, iWidf = %d\n", iWidf);

			for (iLenf = 0; iLenf < sColor_Imagef->nLength; iLenf++)
			{
				nIndexOfPixelCurf = iLenf + (iWidf*sColor_Imagef->nLength); // nLenMax);

				nRedCurf = sColor_Imagef->nRed_Arr[nIndexOfPixelCurf];
				nGreenCurf = sColor_Imagef->nGreen_Arr[nIndexOfPixelCurf];
				nBlueCurf = sColor_Imagef->nBlue_Arr[nIndexOfPixelCurf];
				/*
				if (iWidf >= nWidSubimageToPrintMin && iWidf <= nWidSubimageToPrintMax && iLenf >= nLenSubimageToPrintMin && iLenf <= nLenSubimageToPrintMax)
				{
				fprintf(fout, "\n\n iLenf = %d, iWidf = %d, nRedCurf = %d, nGreenCurf = %d, nBlueCurf = %d", iLenf, iWidf, nRedCurf, nGreenCurf, nBlueCurf);
				fprintf(fout, "\n Prev: nLengthOfTheSamePixelIntensities_ForObjectBoundary_Curf = %d", nLengthOfTheSamePixelIntensities_ForObjectBoundary_Curf);
				}// if (iWidf >= nWidSubimageToPrintMin && iWidf <= nWidSubimageToPrintMax && iLenf >= nLenSubimageToPrintMin && iLenf <= nLenSubimageToPrintMax)
				*/

				//	if (nRedCurf != sColor_Imagef->nIntensityOfBackground_Red || nGreenCurf != sColor_Imagef->nIntensityOfBackground_Green ||
				//	nBlueCurf != sColor_Imagef->nIntensityOfBackground_Blue)
				if (nRedCurf >= nIntensityCloseToBlackMax || nGreenCurf >= nIntensityCloseToBlackMax ||
					nBlueCurf >= nIntensityCloseToBlackMax)
				{
					nLengthOfTheSamePixelIntensities_ForObjectBoundary_Curf = 0;

					continue;
				} // if (nRedCurf >= nIntensityCloseToBlackMax || nGreenCurf >= nIntensityCloseToBlackMax ||...

				nLengthOfTheSamePixelIntensities_ForObjectBoundary_Curf += 1;

				if (nLengthOfTheSamePixelIntensities_ForObjectBoundary_Curf == 1)
				{
					nIs_ExitFromALinePossiblef = 1; //possible
				} // if (nLengthOfTheSamePixelIntensities_ForObjectBoundary_Curf == 1)
				  //new
				if (iWidf > 0 && sColor_Imagef->nLenObjectBoundary_Arr[iWidf - 1] != -1)
				{
					nDiffOfLensf = iLenf - sColor_Imagef->nLenObjectBoundary_Arr[iWidf - 1];

					if (nDiffOfLensf - nLengthOfTheSamePixelIntensities_ForObjectBoundary_Min > nDiffOfLens_Maxf && iWidf > nWidForfNonZeroObjectBoundariesf &&
						nWidForfNonZeroObjectBoundariesf >= 0 && nIs_ExitFromALinePossiblef == 1) // to the right
					{
						fprintf(fout, "\n\n Exit for the right boundary of iWidf = %d, iLenf = %d, prev sColor_Imagef->nLenObjectBoundary_Arr[%d] = %d, nDiffOfLensf = %d, sColor_Imagef->nLength = %d",
							iWidf, iLenf, iWidf - 1, sColor_Imagef->nLenObjectBoundary_Arr[iWidf - 1], nDiffOfLensf, sColor_Imagef->nLength);

						fprintf(fout, "\n Current sColor_Imagef->nLenObjectBoundary_Arr[%d] = %d", iWidf, sColor_Imagef->nLenObjectBoundary_Arr[iWidf]);

						break; // for (iLenf = 0; iLenf < sColor_Imagef->nLength;
					} // if (nDiffOfLensf > nDiffOfLens_Maxf && iWidf > nWidForfNonZeroObjectBoundariesf)

				}// if (iWidf > 0 && ...)

				 //////////////////////////////////////////////////////////////////////////////////////////////////////
				 //for 1st line with object boundary or for a line with no object boundary
				if (iLenf > 0 && (nWidForfNonZeroObjectBoundariesf == -1 || iWidf == nWidForfNonZeroObjectBoundariesf) &&
					nIs_ExitFromALinePossiblef == 1)
				{
					//if (iWidf == nWidForfNonZeroObjectBoundariesf && iLenf >= nDistOf1stObjectPixel_ToImageEdgeMaxf)
					if (iLenf >= nDistOf1stObjectPixel_ToImageEdgeMaxf)
						//&& nIs_ExitFromALinePossiblef == 1) // already included
					{
						fprintf(fout, "\n\n The pixel can not be the 1st object pixel of an object to the left, iWidf = %d, iLenf = %d > nDistOf1stObjectPixel_ToImageEdgeMaxf = %d",
							iWidf, iLenf, nDistOf1stObjectPixel_ToImageEdgeMaxf);
						fprintf(fout, "\n Current sColor_Imagef->nLenObjectBoundary_Arr[%d] = %d", iWidf, sColor_Imagef->nLenObjectBoundary_Arr[iWidf]);

						fprintf(fout, "\n Exit from the line iWidf = %d", iWidf);
						nIs_ExitFromALinePossiblef = 0;
						break; // for (iLenf = 0; iLenf < sColor_Imagef->nLength;
					} // if (iLenf >= nDistOf1stObjectPixel_ToImageEdgeMaxf)

					nResf = CharacteristicsOfNeighborsInARow(

						sColor_Imagef, //const COLOR_IMAGE *sColor_Imagef, //[nImageSizeMax]

						iWidf, //const int nWidCurf,
						iLenf, //const int nLenCurf,
							   /////////////////////////////////////////////////////////////
						nNumOfBlackNeighboingPixelsFrTheSameRowf, //int &nNumOfBlackNeighboingPixelsFrTheSameRowf,
						nAverIntensityOverNeighboingPixelsFrTheSameRowf, //int &nAverIntensityOverNeighboingPixelsFrTheSameRowf,
						nDiffBetweenMaxAndMinf); // int &nDiffBetweenMaxAndMinf);

					fprintf(fout, "\n\n Left object: iWidf = %d, iLenf = %d, nNumOfBlackNeighboingPixelsFrTheSameRowf = %d",
						iWidf, iLenf, nNumOfBlackNeighboingPixelsFrTheSameRowf);
					fprintf(fout, "\n\n nAverIntensityOverNeighboingPixelsFrTheSameRowf = %d, nDiffBetweenMaxAndMinf = %d",
						nAverIntensityOverNeighboingPixelsFrTheSameRowf, nDiffBetweenMaxAndMinf);

					if (nNumOfBlackNeighboingPixelsFrTheSameRowf >= nNumOfBlackNeighboingPixelsFrTheSameRowMax)
					{
						fprintf(fout, "\n\n 1: discarding a possible right boundary by nNumOfBlackNeighboingPixelsFrTheSameRowf = %d >= nNumOfBlackNeighboingPixelsFrTheSameRowMax = %d",
							nNumOfBlackNeighboingPixelsFrTheSameRowf, nNumOfBlackNeighboingPixelsFrTheSameRowMax);

						fprintf(fout, "\n Current iLenf = %d, sColor_Imagef->nLenObjectBoundary_Arr[%d] = %d", iLenf, iWidf, sColor_Imagef->nLenObjectBoundary_Arr[iWidf]);

						nIs_ExitFromALinePossiblef = 0; // the exit from a line is impossible until 'nIs_ExitFromALinePossiblef = 1'
						continue; // for the line
					}//if (nNumOfBlackNeighboingPixelsFrTheSameRowf >= nNumOfBlackNeighboingPixelsFrTheSameRowMax)

					if (nAverIntensityOverNeighboingPixelsFrTheSameRowf <= nAverIntensityOverNeighboingPixelsFrTheSameRowMin)
					{
						fprintf(fout, "\n\n 1: discarding a possible right boundary by nAverIntensityOverNeighboingPixelsFrTheSameRowf = %d <= nAverIntensityOverNeighboingPixelsFrTheSameRowMin = %d",
							nAverIntensityOverNeighboingPixelsFrTheSameRowf, nAverIntensityOverNeighboingPixelsFrTheSameRowMin);

						fprintf(fout, "\n Current iLenf = %d, sColor_Imagef->nLenObjectBoundary_Arr[%d] = %d", iLenf, iWidf, sColor_Imagef->nLenObjectBoundary_Arr[iWidf]);

						nIs_ExitFromALinePossiblef = 0; // the exit from a line is impossible until 'nIs_ExitFromALinePossiblef = 1'
						continue; // for the line
					}//if (nAverIntensityOverNeighboingPixelsFrTheSameRowf <= nAverIntensityOverNeighboingPixelsFrTheSameRowMin)

					 //nNumOfRightBoundariesf += 1;

					if (nWidForfNonZeroObjectBoundariesf == -1)
					{
						/*
						nWidForfNonZeroObjectBoundariesf = iWidf; // inside 'if (nNumOfNonZeroObjectBoundariesf == 1)'

						fprintf(fout, "\n\n The first nonzero next possible right boundary of the object by 'CharacteristicsOfNeighborsInARow': ");

						fprintf(fout, "\n nWidForfNonZeroObjectBoundariesf = %d, the 1st nNumOfRightBoundariesf = %d, iLenf = %d",
						nWidForfNonZeroObjectBoundariesf, nNumOfRightBoundariesf, iLenf);
						*/
						//verifying the 1st possible object boundary: pixels below and/or to the left should be object pixels
						//Below
						nWidBelowf = iWidf;

						//MarkAddWidForLeft:				
						nWidBelowf = nWidBelowf + nWidDiffFor1stObjectPixelVerification;

						if (nWidBelowf >= sColor_Imagef->nWidth)
						{
							printf("\n\n An error in verification of the 1st object pixel, nWidBelowf = %d >= sColor_Imagef->nWidth = %d, iWidf = %d, iLenf = %d",
								nWidBelowf, sColor_Imagef->nWidth, iWidf, iLenf);
							fprintf(fout, "\n\n An error in verification of the 1st object pixel, nWidBelowf = %d >= sColor_Imagef->nWidth = %d, iWidf = %d, iLenf = %d",
								nWidBelowf, sColor_Imagef->nWidth, iWidf, iLenf);

							printf("\n\n Please adjust the settings and press any key to exit");
							getchar(); exit(1);
						} // if (nWidBelowf >= sColor_Imagef->nWidth)

						  //nIndexOfPixelBelowf = nLenOfObjectBoundaryCurf + (nWidBelowf*sColor_Imagef->nLength); // nLenMax);
						nIndexOfPixelBelowf = iLenf + (nWidBelowf*sColor_Imagef->nLength); // nLenMax);

						nRedBelowf = sColor_Imagef->nRed_Arr[nIndexOfPixelBelowf];
						nGreenBelowf = sColor_Imagef->nGreen_Arr[nIndexOfPixelBelowf];
						nBlueBelowf = sColor_Imagef->nBlue_Arr[nIndexOfPixelBelowf];

						fprintf(fout, "\n\n Below: iLenf = %d, nWidBelowf = %d, nRedBelowf = %d, nGreenBelowf = %d, nBlueBelowf = %d",
							iLenf, nWidBelowf, nRedBelowf, nGreenBelowf, nBlueBelowf);

						if (nRedBelowf >= nIntensityCloseToBlackMax || nGreenBelowf >= nIntensityCloseToBlackMax ||
							nBlueBelowf >= nIntensityCloseToBlackMax)
						{
							fprintf(fout, "\n\n The 1st right boundary is verified by an object pixel below");
							goto Mark1stObjectPixel_LeftObject;
						} // if (nRedBelowf != sColor_Imagef->nIntensityOfBackground_Red || ...
						else // if (nRedBelowf < nIntensityCloseToBlackMax && nGreenBelowf < nIntensityCloseToBlackMax...
						{
							//the pixel to the left for the left object
							nLenSidef = iLenf - nLenDiffFor1stObjectPixelVerification;
							//nLenSidef = iLenf + nLenDiffFor1stObjectPixelVerification;

							fprintf(fout, "\n\n Checking up if the side pixel to the right belongs to the object, iWidf = %d, nLenSidef = %d, iLenf = %d",
								iWidf, nLenSidef, iLenf);

							if (nLenSidef < 0)
								//if (nLenSidef > sColor_Imagef->nLength - 1)
							{
								nLenSidef = 0; // sColor_Imagef->nLength - 1;
								fprintf(fout, "\n The distance to the left image edge < nLenDiffFor1stObjectPixelVerification");
								//nLenSidef = 0;
								//fprintf(fout, "\n The distance to the left image edge < nLenDiffFor1stObjectPixelVerification");

							} //if (nLenSidef < 0)

							nIndexOfPixelSidef = nLenSidef + (iWidf*sColor_Imagef->nLength); // nLenMax);
							nRedSidef = sColor_Imagef->nRed_Arr[nIndexOfPixelSidef];
							nGreenSidef = sColor_Imagef->nGreen_Arr[nIndexOfPixelSidef];
							nBlueSidef = sColor_Imagef->nBlue_Arr[nIndexOfPixelSidef];

							fprintf(fout, "\n\n Left side: iLenf = %d, iWidf = %d, nLenSidef = %d, nRedSidef = %d, nGreenSidef = %d, nBlueSidef = %d",
								iLenf, iWidf, nLenSidef, nRedSidef, nGreenSidef, nBlueSidef);

							if (nRedSidef >= nIntensityCloseToBlackMax || nGreenSidef >= nIntensityCloseToBlackMax ||
								nBlueSidef >= nIntensityCloseToBlackMax)
							{
								fprintf(fout, "\n\n The 1st right boundary is verified by an object pixel to the left side");
								goto Mark1stObjectPixel_LeftObject;
							} // if (nRedSidef != sColor_Imagef->nIntensityOfBackground_Red || ...
							else
							{
								fprintf(fout, "\n\n The 1st right boundary is Not verified by object pixels both below and to the left side");
								goto Mark_No_1stObjectPixel_LeftObject;
							} //

						} // else //if (nRedBelowf < nIntensityCloseToBlackMax && nGreenBelowf < nIntensityCloseToBlackMax...

					Mark_No_1stObjectPixel_LeftObject: nNumOfNonZeroObjectBoundariesf = 0;

						nIsObjectPixelBelowf = 0;

						fprintf(fout, "\n\n The pixel can not be the 1st object pixel by a background below and to the left, iWidf = %d, iLenf = %d < nDistOf1stObjectPixel_ToImageEdgeMaxf = %d",
							iWidf, iLenf, nDistOf1stObjectPixel_ToImageEdgeMaxf);

						fprintf(fout, "\n Current sColor_Imagef->nLenObjectBoundary_Arr[%d] = %d (object to the left), iLenf = %d",
							iWidf, sColor_Imagef->nLenObjectBoundary_Arr[iWidf], iLenf);

						continue;
						//break; // for (iLenf = 0; iLenf < sColor_Imagef->nLength;
						///////////////////////////////////////////////////

					Mark1stObjectPixel_LeftObject:	nWidForfNonZeroObjectBoundariesf = iWidf; // inside 'if (nNumOfNonZeroObjectBoundariesf == 1)'
						nNumOfRightBoundariesf += 1;

						fprintf(fout, "\n\n The first nonzero next possible right boundary of the object by 'CharacteristicsOfNeighborsInARow': ");

						fprintf(fout, "\n nWidForfNonZeroObjectBoundariesf = %d, the 1st nNumOfRightBoundariesf = %d, iLenf = %d",
							nWidForfNonZeroObjectBoundariesf, nNumOfRightBoundariesf, iLenf);

					} //if (nWidForfNonZeroObjectBoundariesf == -1)

					sColor_Imagef->nLenObjectBoundary_Arr[iWidf] = iLenf - 1;

					if (nNumOfNonZeroObjectBoundariesf == 0)
					{
						nNumOfNonZeroObjectBoundariesf = 1;
					}//if (nNumOfNonZeroObjectBoundariesf == 0)

					fprintf(fout, "\n\n The next possible right boundary of the object in the 1st object line by 'CharacteristicsOfNeighborsInARow':");

					fprintf(fout, "\n sColor_Imagef->nLenObjectBoundary_Arr[%d] = %d, a new nNumOfRightBoundariesf = %d, sColor_Imagef->nLength = %d",
						iWidf, sColor_Imagef->nLenObjectBoundary_Arr[iWidf], nNumOfRightBoundariesf, sColor_Imagef->nLength);

					nIs_ExitFromALinePossiblef = 0;
					continue;
				} //if (iLenf < sColor_Imagef->nLength - 1 && ( nWidForfNonZeroObjectBoundariesf == -1 || ... 

				  ///////////////////////////
				  //if (nLengthOfTheSamePixelIntensities_ForObjectBoundary_Curf == nLengthOfTheSamePixelIntensities_ForObjectBoundary_Min)
				if (iLenf > 0 && nIs_ExitFromALinePossiblef == 1 && nWidForfNonZeroObjectBoundariesf != -1 &&
					iWidf > nWidForfNonZeroObjectBoundariesf)
				{
					//nLenOfObjectBoundaryCurf = iLenf - nLengthOfTheSamePixelIntensities_ForObjectBoundary_Min;
					nLenOfObjectBoundaryCurf = iLenf - 1;

					if (nLenOfObjectBoundaryCurf < 0)
						nLenOfObjectBoundaryCurf = 0;

					if (iWidf > 0 && sColor_Imagef->nLenObjectBoundary_Arr[iWidf - 1] != -1)
					{
						nDiffOfLensForBoundariesf = nLenOfObjectBoundaryCurf - sColor_Imagef->nLenObjectBoundary_Arr[iWidf - 1];

						//if (nDiffOfLensf > nDistBetweenBoundariesOfNeighborWids_Max && nNumOfNonZeroObjectBoundariesf > 0) // to the right nWidForfNonZeroObjectBoundariesf = iWidf
						if (nDiffOfLensForBoundariesf > nDistBetweenBoundariesOfNeighborWids_Max && iWidf > nWidForfNonZeroObjectBoundariesf) // to the right  
							//&& nIs_ExitFromALinePossiblef == 1) // already included
						{
							fprintf(fout, "\n\n Discarding a possible right boundary: iWidf = %d, nLenOfObjectBoundaryCurf = %d, prev sColor_Imagef->nLenObjectBoundary_Arr[%d] = %d, nDiffOfLensForBoundariesf = %d, sColor_Imagef->nLength = %d",
								iWidf, nLenOfObjectBoundaryCurf, iWidf - 1, sColor_Imagef->nLenObjectBoundary_Arr[iWidf - 1], nDiffOfLensForBoundariesf, sColor_Imagef->nLength);

							fprintf(fout, "\n Current sColor_Imagef->nLenObjectBoundary_Arr[%d] = %d", iWidf, sColor_Imagef->nLenObjectBoundary_Arr[iWidf]);

							nIs_ExitFromALinePossiblef = 0;
							break; // for (iLenf = 0; iLenf < sColor_Imagef->nLength;
						} // if (nDiffOfLensForBoundariesf > nDistBetweenBoundariesOfNeighborWids_Max && iWidf > nWidForfNonZeroObjectBoundariesf) 

					}// if (iWidf > 0 && sColor_Imagef->nLenObjectBoundary_Arr[iWidf - 1] != -1)

					if (nLenOfObjectBoundaryCurf > 0) //nLenOfObjectBoundaryCurf = iLenf - nLengthOfTheSamePixelIntensities_ForObjectBoundary_Min;
					{
						nResf = CharacteristicsOfNeighborsInARow(

							sColor_Imagef, //const COLOR_IMAGE *sColor_Imagef, //[nImageSizeMax]

							iWidf, //const int nWidCurf,
							nLenOfObjectBoundaryCurf, //const int nLenCurf,
													  /////////////////////////////////////////////////////////////
							nNumOfBlackNeighboingPixelsFrTheSameRowf, //int &nNumOfBlackNeighboingPixelsFrTheSameRowf,
							nAverIntensityOverNeighboingPixelsFrTheSameRowf, //int &nAverIntensityOverNeighboingPixelsFrTheSameRowf,
							nDiffBetweenMaxAndMinf); // int &nDiffBetweenMaxAndMinf);

						fprintf(fout, "\n\n iWidf = %d, nLenOfObjectBoundaryCurf = %d, nNumOfBlackNeighboingPixelsFrTheSameRowf = %d",
							iWidf, nLenOfObjectBoundaryCurf, nNumOfBlackNeighboingPixelsFrTheSameRowf);
						fprintf(fout, "\n\n nAverIntensityOverNeighboingPixelsFrTheSameRowf = %d, nDiffBetweenMaxAndMinf = %d",
							nAverIntensityOverNeighboingPixelsFrTheSameRowf, nDiffBetweenMaxAndMinf);

						if (nNumOfBlackNeighboingPixelsFrTheSameRowf >= nNumOfBlackNeighboingPixelsFrTheSameRowMax)
						{
							fprintf(fout, "\n\n 2: discarding a possible right boundary by nNumOfBlackNeighboingPixelsFrTheSameRowf = %d >= nNumOfBlackNeighboingPixelsFrTheSameRowMax = %d",
								nNumOfBlackNeighboingPixelsFrTheSameRowf, nNumOfBlackNeighboingPixelsFrTheSameRowMax);

							fprintf(fout, "\n Current nLenOfObjectBoundaryCurf = %d, sColor_Imagef->nLenObjectBoundary_Arr[%d] = %d", nLenOfObjectBoundaryCurf, iWidf, sColor_Imagef->nLenObjectBoundary_Arr[iWidf]);
							nIs_ExitFromALinePossiblef = 0;
							continue;
							//break; //
						}//if (nNumOfBlackNeighboingPixelsFrTheSameRowf >= nNumOfBlackNeighboingPixelsFrTheSameRowMax)

						if (nAverIntensityOverNeighboingPixelsFrTheSameRowf <= nAverIntensityOverNeighboingPixelsFrTheSameRowMin)
						{
							fprintf(fout, "\n\n 2: discarding a possible right boundary by nAverIntensityOverNeighboingPixelsFrTheSameRowf = %d <= nAverIntensityOverNeighboingPixelsFrTheSameRowMin = %d",
								nAverIntensityOverNeighboingPixelsFrTheSameRowf, nAverIntensityOverNeighboingPixelsFrTheSameRowMin);

							fprintf(fout, "\n Current nLenOfObjectBoundaryCurf = %d, sColor_Imagef->nLenObjectBoundary_Arr[%d] = %d", nLenOfObjectBoundaryCurf, iWidf, sColor_Imagef->nLenObjectBoundary_Arr[iWidf]);
							nIs_ExitFromALinePossiblef = 0;
							continue;
							//break; //
						}//if (nAverIntensityOverNeighboingPixelsFrTheSameRowf <= nAverIntensityOverNeighboingPixelsFrTheSameRowMin)
					} //if (nLenOfObjectBoundaryCurf > 0)

					nNumOfRightBoundariesf += 1;

					nNumOfNonZeroObjectBoundariesf += 1;
					sColor_Imagef->nLenObjectBoundary_Arr[iWidf] = nLenOfObjectBoundaryCurf;

					fprintf(fout, "\n\n The next possible right boundary of the object: sColor_Imagef->nLenObjectBoundary_Arr[%d] = %d, a new nNumOfRightBoundariesf = %d, sColor_Imagef->nLength = %d",
						iWidf, sColor_Imagef->nLenObjectBoundary_Arr[iWidf], nNumOfRightBoundariesf, sColor_Imagef->nLength);

					if (nNumOfRightBoundariesf == 1)
					{
						nLenOf_1stObjectBoundaryCurf = nLenOfObjectBoundaryCurf;
					} //if (nNumOfRightBoundariesf == 1)
					else if (nNumOfRightBoundariesf == 2)
					{
						nLenOf_2ndObjectBoundaryCurf = nLenOfObjectBoundaryCurf;
					} //if (nNumOfRightBoundariesf == 2)

				} // if (iLenf > 0 && nIs_ExitFromALinePossiblef == 1 && nWidForfNonZeroObjectBoundariesf != -1 && ...

				  //////////////////////////
				if (iLenf == sColor_Imagef->nLength - 1 && sColor_Imagef->nLenObjectBoundary_Arr[iWidf] == -1)
				{
					sColor_Imagef->nLenObjectBoundary_Arr[iWidf] = 0;

					nNumOfRightBoundariesf += 1;
					sColor_Imagef->nLenObjectBoundary_Arr[iWidf] = 0;

					fprintf(fout, "\n\n The right edge of the image: sColor_Imagef->nLenObjectBoundary_Arr[%d] = %d, nNumOfRightBoundariesf = %d, sColor_Imagef->nLength = %d",
						iWidf, sColor_Imagef->nLenObjectBoundary_Arr[iWidf], nNumOfRightBoundariesf, sColor_Imagef->nLength);

					fprintf(fout, "\n iWidf = %d, iLenf = %d", iWidf, iLenf);

					fprintf(fout, "\n\n nLengthOfTheSamePixelIntensities_ForObjectBoundary_Curf = %d, nLengthOfTheSamePixelIntensities_ForObjectBoundary_Min = %d",
						nLengthOfTheSamePixelIntensities_ForObjectBoundary_Curf, nLengthOfTheSamePixelIntensities_ForObjectBoundary_Min);

				} // if (iLenf == sColor_Imagef->nLength - 1 && sColor_Imagef->nLenObjectBoundary_Arr[iWidf] == -1)

			} //for (iLenf = 0; iLenf < sColor_Imagef->nLength; iLenf++)

			if (nWidForfNonZeroObjectBoundariesf == -1)
			{
				sColor_Imagef->nLenObjectBoundary_Arr[iWidf] = 0;

				fprintf(fout, "\n\n nWidForfNonZeroObjectBoundariesf == -1: sColor_Imagef->nLenObjectBoundary_Arr[%d] is adjusted to %d", iWidf, sColor_Imagef->nLenObjectBoundary_Arr[iWidf]);
			} //if (nWidForfNonZeroObjectBoundariesf == -1)

			fprintf(fout, "\n\n Final current sColor_Imagef->nLenObjectBoundary_Arr[%d] = %d", iWidf, sColor_Imagef->nLenObjectBoundary_Arr[iWidf]);

		} //for (iWidf = 0; iWidf < sColor_Imagef->nWidth; iWidf++)
	} // if (sColor_Imagef->nSideOfObjectLocation == -1)

	  //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	  //object to the right
	if (sColor_Imagef->nSideOfObjectLocation == 1)
	{
		fprintf(fout, "\n\n The object is to the right");

		nWidForfNonZeroObjectBoundariesf = -1; // initially
		for (iWidf = 0; iWidf < sColor_Imagef->nWidth; iWidf++)
		{
			if (iWidf > 0 && sColor_Imagef->nLenObjectBoundary_Arr[iWidf - 1] == -1)
			{
				fprintf(fout, "\n\n An error: the object boundary has not been found at iWidf - 1 = %d", iWidf - 1);
				printf("\n\n An error: the object boundary has not been found at iWidf - 1 = %d", iWidf - 1);

				printf("\n\n Please adjust the settings and press any key to exit");
				getchar(); exit(1);
			} //if (iWidf > 0 && sColor_Imagef->nLenObjectBoundary_Arr[iWidf - 1] == -1)

			nNumOfLeftBoundariesf = 0;
			nLengthOfTheSamePixelIntensities_ForObjectBoundary_Curf = 0;

			nIs_ExitFromALinePossiblef = 0; //initially
			nLenOf_1stObjectBoundaryCurf = -1;
			nLenOf_2ndObjectBoundaryCurf = -1;

			fprintf(fout, "\n\n The length boundaries for a right object, iWidf = %d\n", iWidf);

			for (iLenf = sColor_Imagef->nLength - 1; iLenf >= 0; iLenf--)
			{
				nIndexOfPixelCurf = iLenf + (iWidf*sColor_Imagef->nLength); // nLenMax);

				nRedCurf = sColor_Imagef->nRed_Arr[nIndexOfPixelCurf];
				nGreenCurf = sColor_Imagef->nGreen_Arr[nIndexOfPixelCurf];
				nBlueCurf = sColor_Imagef->nBlue_Arr[nIndexOfPixelCurf];

				if (iWidf >= nWidSubimageToPrintMin && iWidf <= nWidSubimageToPrintMax && iLenf >= nLenSubimageToPrintMin && iLenf <= nLenSubimageToPrintMax)
				{
					fprintf(fout, "\n\n iLenf = %d, iWidf = %d, nRedCurf = %d, nGreenCurf = %d, nBlueCurf = %d", iLenf, iWidf, nRedCurf, nGreenCurf, nBlueCurf);

					//fprintf(fout, "\n Prev: nLengthOfTheSamePixelIntensities_ForObjectBoundary_Curf = %d, nDiffOfLensf = %d", nLengthOfTheSamePixelIntensities_ForObjectBoundary_Curf, nDiffOfLensf);
					fprintf(fout, "\n Prev: nLengthOfTheSamePixelIntensities_ForObjectBoundary_Curf = %d", nLengthOfTheSamePixelIntensities_ForObjectBoundary_Curf);
				}// if (iWidf >= nWidSubimageToPrintMin && iWidf <= nWidSubimageToPrintMax && iLenf >= nLenSubimageToPrintMin && iLenf <= nLenSubimageToPrintMax)

				 //	if (nRedCurf != sColor_Imagef->nIntensityOfBackground_Red || nGreenCurf != sColor_Imagef->nIntensityOfBackground_Green ||
				 //	nBlueCurf != sColor_Imagef->nIntensityOfBackground_Blue)
				if (nRedCurf >= nIntensityCloseToBlackMax || nGreenCurf >= nIntensityCloseToBlackMax ||
					nBlueCurf >= nIntensityCloseToBlackMax)
				{
					nLengthOfTheSamePixelIntensities_ForObjectBoundary_Curf = 0;

					continue;
				} // if (nRedCurf >= nIntensityCloseToBlackMax || nGreenCurf >= nIntensityCloseToBlackMax ||...

				nLengthOfTheSamePixelIntensities_ForObjectBoundary_Curf += 1;

				if (nLengthOfTheSamePixelIntensities_ForObjectBoundary_Curf == 1)
				{
					nIs_ExitFromALinePossiblef = 1; //possible
				} // if (nLengthOfTheSamePixelIntensities_ForObjectBoundary_Curf == 1)

				if (iWidf > 0 && sColor_Imagef->nLenObjectBoundary_Arr[iWidf - 1] != -1)
				{
					nDiffOfLensf = sColor_Imagef->nLenObjectBoundary_Arr[iWidf - 1] - iLenf;

					//if (nDiffOfLensf > nDiffOfLens_Maxf && iWidf > nWidForfNonZeroObjectBoundariesf)
					if (nDiffOfLensf - nLengthOfTheSamePixelIntensities_ForObjectBoundary_Min > nDiffOfLens_Maxf && iWidf > nWidForfNonZeroObjectBoundariesf &&
						nWidForfNonZeroObjectBoundariesf >= 0 && nIs_ExitFromALinePossiblef == 1)
					{
						fprintf(fout, "\n\n Exit for the left boundary of iWidf = %d, iLenf = %d, prev sColor_Imagef->nLenObjectBoundary_Arr[%d] = %d, nDiffOfLensf = %d, sColor_Imagef->nLength = %d",
							iWidf, iLenf, iWidf - 1, sColor_Imagef->nLenObjectBoundary_Arr[iWidf - 1], nDiffOfLensf, sColor_Imagef->nLength);

						fprintf(fout, "\n Current sColor_Imagef->nLenObjectBoundary_Arr[%d] = %d", iWidf, sColor_Imagef->nLenObjectBoundary_Arr[iWidf]);

						break; // for (iLenf = sColor_Imagef->nLength - 1; iLenf >= 0; iLenf--)

					} // if (nDiffOfLensf - nLengthOfTheSamePixelIntensities_ForObjectBoundary_Min > nDiffOfLens_Maxf && ...

				} //if (iWidf > 0 && sColor_Imagef->nLenObjectBoundary_Arr[iWidf - 1] != -1)
				  //////////////////////////////////////////////////////////////////////////////////////////////////////

				  //for 1st line with object boundary or for a line with no object boundary
				if (iLenf < sColor_Imagef->nLength - 1 && (nWidForfNonZeroObjectBoundariesf == -1 || iWidf == nWidForfNonZeroObjectBoundariesf) &&
					nIs_ExitFromALinePossiblef == 1)
				{
					//if (iWidf == nWidForfNonZeroObjectBoundariesf && iLenf <= sColor_Imagef->nLength - nDistOf1stObjectPixel_ToImageEdgeMaxf)
					if (iLenf <= sColor_Imagef->nLength - nDistOf1stObjectPixel_ToImageEdgeMaxf)
						//&& nIs_ExitFromALinePossiblef == 1) // already included
					{
						fprintf(fout, "\n\n The pixel can not be the 1st object pixel of an object to the right, iWidf = %d, iLenf = %d <= sColor_Imagef->nLength - nDistOf1stObjectPixel_ToImageEdgeMaxf = %d",
							iWidf, iLenf, sColor_Imagef->nLength - nDistOf1stObjectPixel_ToImageEdgeMaxf);

						fprintf(fout, "\n Current sColor_Imagef->nLenObjectBoundary_Arr[%d] = %d", iWidf, sColor_Imagef->nLenObjectBoundary_Arr[iWidf]);

						fprintf(fout, "\n Exit from the line iWidf = %d", iWidf);

						nIs_ExitFromALinePossiblef = 0;
						break; // for (iLenf = sColor_Imagef->nLength - 1; iLenf >= 0; iLenf--)
					} // if (iLenf <= sColor_Imagef->nLength - nDistOf1stObjectPixel_ToImageEdgeMaxf && ...)

					nResf = CharacteristicsOfNeighborsInARow(

						sColor_Imagef, //const COLOR_IMAGE *sColor_Imagef, //[nImageSizeMax]

						iWidf, //const int nWidCurf,
						iLenf, //const int nLenCurf,
							   /////////////////////////////////////////////////////////////
						nNumOfBlackNeighboingPixelsFrTheSameRowf, //int &nNumOfBlackNeighboingPixelsFrTheSameRowf,
						nAverIntensityOverNeighboingPixelsFrTheSameRowf, //int &nAverIntensityOverNeighboingPixelsFrTheSameRowf,
						nDiffBetweenMaxAndMinf); // int &nDiffBetweenMaxAndMinf);

					fprintf(fout, "\n\n Right object: iWidf = %d, iLenf = %d, nNumOfBlackNeighboingPixelsFrTheSameRowf = %d",
						iWidf, iLenf, nNumOfBlackNeighboingPixelsFrTheSameRowf);
					fprintf(fout, "\n\n nAverIntensityOverNeighboingPixelsFrTheSameRowf = %d, nDiffBetweenMaxAndMinf = %d",
						nAverIntensityOverNeighboingPixelsFrTheSameRowf, nDiffBetweenMaxAndMinf);

					if (nNumOfBlackNeighboingPixelsFrTheSameRowf >= nNumOfBlackNeighboingPixelsFrTheSameRowMax)
					{
						fprintf(fout, "\n\n 1: discarding a possible left boundary by nNumOfBlackNeighboingPixelsFrTheSameRowf = %d >= nNumOfBlackNeighboingPixelsFrTheSameRowMax = %d",
							nNumOfBlackNeighboingPixelsFrTheSameRowf, nNumOfBlackNeighboingPixelsFrTheSameRowMax);

						fprintf(fout, "\n Current iLenf = %d, sColor_Imagef->nLenObjectBoundary_Arr[%d] = %d", iLenf, iWidf, sColor_Imagef->nLenObjectBoundary_Arr[iWidf]);

						nIs_ExitFromALinePossiblef = 0; // the exit from a line is impossible until 'nIs_ExitFromALinePossiblef = 1'
						continue; // for the line
					}//if (nNumOfBlackNeighboingPixelsFrTheSameRowf >= nNumOfBlackNeighboingPixelsFrTheSameRowMax)

					if (nAverIntensityOverNeighboingPixelsFrTheSameRowf <= nAverIntensityOverNeighboingPixelsFrTheSameRowMin)
					{
						fprintf(fout, "\n\n 1: discarding a possible left boundary by nAverIntensityOverNeighboingPixelsFrTheSameRowf = %d <= nAverIntensityOverNeighboingPixelsFrTheSameRowMin = %d",
							nAverIntensityOverNeighboingPixelsFrTheSameRowf, nAverIntensityOverNeighboingPixelsFrTheSameRowMin);

						fprintf(fout, "\n Current iLenf = %d, sColor_Imagef->nLenObjectBoundary_Arr[%d] = %d", iLenf, iWidf, sColor_Imagef->nLenObjectBoundary_Arr[iWidf]);

						nIs_ExitFromALinePossiblef = 0; // the exit from a line is impossible until 'nIs_ExitFromALinePossiblef = 1'
						continue; // for the line
					}//if (nAverIntensityOverNeighboingPixelsFrTheSameRowf <= nAverIntensityOverNeighboingPixelsFrTheSameRowMin)
					 //////////////////////////////////////////////////////////////////////

					if (nWidForfNonZeroObjectBoundariesf == -1)
					{
						//verifying the 1st possible object boundary: pixels below and/or to the rightt should be object pixels
						//Below
						nWidBelowf = iWidf;

						//MarkAddWidForLeft:				
						nWidBelowf = nWidBelowf + nWidDiffFor1stObjectPixelVerification;

						if (nWidBelowf >= sColor_Imagef->nWidth)
						{
							printf("\n\n An error in verification of the 1st object pixel, nWidBelowf = %d >= sColor_Imagef->nWidth = %d, iWidf = %d, iLenf = %d",
								nWidBelowf, sColor_Imagef->nWidth, iWidf, iLenf);
							fprintf(fout, "\n\n An error in verification of the 1st object pixel, nWidBelowf = %d >= sColor_Imagef->nWidth = %d, iWidf = %d, iLenf = %d",
								nWidBelowf, sColor_Imagef->nWidth, iWidf, iLenf);

							printf("\n\n Please adjust the settings and press any key to exit");
							getchar(); exit(1);
						} // if (nWidBelowf >= sColor_Imagef->nWidth)

						  //nIndexOfPixelBelowf = nLenOfObjectBoundaryCurf + (nWidBelowf*sColor_Imagef->nLength); // nLenMax);
						nIndexOfPixelBelowf = iLenf + (nWidBelowf*sColor_Imagef->nLength); // nLenMax);

						nRedBelowf = sColor_Imagef->nRed_Arr[nIndexOfPixelBelowf];
						nGreenBelowf = sColor_Imagef->nGreen_Arr[nIndexOfPixelBelowf];
						nBlueBelowf = sColor_Imagef->nBlue_Arr[nIndexOfPixelBelowf];

						fprintf(fout, "\n\n Below: iLenf = %d, nWidBelowf = %d, nRedBelowf = %d, nGreenBelowf = %d, nBlueBelowf = %d",
							iLenf, nWidBelowf, nRedBelowf, nGreenBelowf, nBlueBelowf);

						if (nRedBelowf >= nIntensityCloseToBlackMax || nGreenBelowf >= nIntensityCloseToBlackMax ||
							nBlueBelowf >= nIntensityCloseToBlackMax)
						{
							fprintf(fout, "\n\n The 1st left boundary is verified by an object pixel below");
							goto Mark1stObjectPixel_RightObject;
						} // if (nRedBelowf != sColor_Imagef->nIntensityOfBackground_Red || ...
						else // if (nRedBelowf < nIntensityCloseToBlackMax && nGreenBelowf < nIntensityCloseToBlackMax...
						{
							//the pixel to the right for the right object
							//nLenSidef = iLenf - nLenDiffFor1stObjectPixelVerification;
							nLenSidef = iLenf + nLenDiffFor1stObjectPixelVerification;

							fprintf(fout, "\n\n Checking up if the side pixel to the right belongs to the object, iWidf = %d, nLenSidef = %d, iLenf = %d",
								iWidf, nLenSidef, iLenf);

							//if (nLenSidef < 0)
							if (nLenSidef > sColor_Imagef->nLength - 1)
							{
								nLenSidef = sColor_Imagef->nLength - 1;
								fprintf(fout, "\n The distance to the right image edge < nLenDiffFor1stObjectPixelVerification");
								//nLenSidef = 0;
								//fprintf(fout, "\n The distance to the left image edge < nLenDiffFor1stObjectPixelVerification");

							} //if (nLenSidef > sColor_Imagef->nLength - 1)

							nIndexOfPixelSidef = nLenSidef + (iWidf*sColor_Imagef->nLength); // nLenMax);
							nRedSidef = sColor_Imagef->nRed_Arr[nIndexOfPixelSidef];
							nGreenSidef = sColor_Imagef->nGreen_Arr[nIndexOfPixelSidef];
							nBlueSidef = sColor_Imagef->nBlue_Arr[nIndexOfPixelSidef];

							fprintf(fout, "\n\n Right side: iLenf = %d, iWidf = %d, nLenSidef = %d, nRedSidef = %d, nGreenSidef = %d, nBlueSidef = %d",
								iLenf, iWidf, nLenSidef, nRedSidef, nGreenSidef, nBlueSidef);

							if (nRedSidef >= nIntensityCloseToBlackMax || nGreenSidef >= nIntensityCloseToBlackMax ||
								nBlueSidef >= nIntensityCloseToBlackMax)
							{
								fprintf(fout, "\n\n The 1st left boundary is verified by an object pixel to the right side");
								goto Mark1stObjectPixel_RightObject;
							} // if (nRedSidef != sColor_Imagef->nIntensityOfBackground_Red || ...
							else
							{
								fprintf(fout, "\n\n The 1st left boundary is Not verified by object pixels both below and to the right side");
								goto Mark_No_1stObjectPixel_RightObject;
							} //

						} // else //if (nRedBelowf < nIntensityCloseToBlackMax && nGreenBelowf < nIntensityCloseToBlackMax...

					Mark_No_1stObjectPixel_RightObject: nNumOfNonZeroObjectBoundariesf = 0;

						nIsObjectPixelBelowf = 0;

						fprintf(fout, "\n\n The pixel can not be the 1st object pixel by a background below, iWidf = %d, iLenf = %d < nDistOf1stObjectPixel_ToImageEdgeMaxf = %d",
							iWidf, iLenf, nDistOf1stObjectPixel_ToImageEdgeMaxf);

						fprintf(fout, "\n Current sColor_Imagef->nLenObjectBoundary_Arr[%d] = %d (object to the right), iLenf = %d",
							iWidf, sColor_Imagef->nLenObjectBoundary_Arr[iWidf], iLenf);

						continue;
						//break; // for (iLenf = 0; iLenf < sColor_Imagef->nLength;
						///////////////////////////////////////////////////

					Mark1stObjectPixel_RightObject:	nWidForfNonZeroObjectBoundariesf = iWidf; // inside 'if (nNumOfNonZeroObjectBoundariesf == 1)'
						nNumOfLeftBoundariesf += 1;

						fprintf(fout, "\n\n The first nonzero next possible left boundary of the object by 'CharacteristicsOfNeighborsInARow': ");

						fprintf(fout, "\n nWidForfNonZeroObjectBoundariesf = %d, the 1st nNumOfLeftBoundariesf = %d, iLenf = %d",
							nWidForfNonZeroObjectBoundariesf, nNumOfLeftBoundariesf, iLenf);

					} //if (nWidForfNonZeroObjectBoundariesf == -1)

					  //sColor_Imagef->nLenObjectBoundary_Arr[iWidf] = iLenf - 1; 
					sColor_Imagef->nLenObjectBoundary_Arr[iWidf] = iLenf + 1;

					if (nNumOfNonZeroObjectBoundariesf == 0)
					{
						nNumOfNonZeroObjectBoundariesf = 1;
					}//if (nNumOfNonZeroObjectBoundariesf == 0)

					fprintf(fout, "\n\n The next possible left boundary of the object in the 1st object line by 'CharacteristicsOfNeighborsInARow':");

					fprintf(fout, "\n sColor_Imagef->nLenObjectBoundary_Arr[%d] = %d, a new nNumOfLeftBoundariesf = %d, sColor_Imagef->nLength = %d",
						iWidf, sColor_Imagef->nLenObjectBoundary_Arr[iWidf], nNumOfLeftBoundariesf, sColor_Imagef->nLength);

					nIs_ExitFromALinePossiblef = 0;
					continue;
				} //if (iLenf < sColor_Imagef->nLength - 1 && ( nWidForfNonZeroObjectBoundariesf == -1 || ... 

				  ///////////////////////////////////////////////////////////////////////////////////////////////////////////
				  //if (nLengthOfTheSamePixelIntensities_ForObjectBoundary_Curf == nLengthOfTheSamePixelIntensities_ForObjectBoundary_Min && 
				if (iLenf < sColor_Imagef->nLength - 1 && nIs_ExitFromALinePossiblef == 1 && nWidForfNonZeroObjectBoundariesf != -1 &&
					iWidf > nWidForfNonZeroObjectBoundariesf)
				{
					//nLenOfObjectBoundaryCurf = iLenf + nLengthOfTheSamePixelIntensities_ForObjectBoundary_Min;
					nLenOfObjectBoundaryCurf = iLenf + 1;

					if (nLenOfObjectBoundaryCurf > sColor_Imagef->nLength - 1)
						nLenOfObjectBoundaryCurf = sColor_Imagef->nLength - 1;

					if (iWidf > 0 && sColor_Imagef->nLenObjectBoundary_Arr[iWidf - 1] != -1)
					{
						//if (iWidf == nWidForfNonZeroObjectBoundariesf && iLenf >= nDistOf1stObjectPixel_ToImageEdgeMaxf)

						nDiffOfLensForBoundariesf = sColor_Imagef->nLenObjectBoundary_Arr[iWidf - 1] - nLenOfObjectBoundaryCurf;

						//if (nDiffOfLensf > nDistBetweenBoundariesOfNeighborWids_Max && nNumOfNonZeroObjectBoundariesf > 0) // to the right nWidForfNonZeroObjectBoundariesf = iWidf
						if (nDiffOfLensForBoundariesf > nDistBetweenBoundariesOfNeighborWids_Max && iWidf > nWidForfNonZeroObjectBoundariesf)
							//&& nIs_ExitFromALinePossiblef == 1) // already included
						{
							fprintf(fout, "\n\n Discarding a possible left boundary: iWidf = %d, nLenOfObjectBoundaryCurf = %d, prev sColor_Imagef->nLenObjectBoundary_Arr[%d] = %d, nDiffOfLensForBoundariesf = %d, sColor_Imagef->nLength = %d",
								iWidf, nLenOfObjectBoundaryCurf, iWidf - 1, sColor_Imagef->nLenObjectBoundary_Arr[iWidf - 1], nDiffOfLensForBoundariesf, sColor_Imagef->nLength);

							fprintf(fout, "\n Current sColor_Imagef->nLenObjectBoundary_Arr[%d] = %d", iWidf, sColor_Imagef->nLenObjectBoundary_Arr[iWidf]);

							nIs_ExitFromALinePossiblef = 0;
							continue;
							//break; // for (iLenf = sColor_Imagef->nLength - 1; iLenf >= 0; iLenf--)
						} // if (nDiffOfLensForBoundariesf > nDistBetweenBoundariesOfNeighborWids_Max && iWidf > nWidForfNonZeroObjectBoundariesf && ...)

					}// if (iWidf > 0 && && sColor_Imagef->nLenObjectBoundary_Arr[iWidf - 1] != -1)

					if (nLenOfObjectBoundaryCurf < sColor_Imagef->nLength - 1) // nLenOfObjectBoundaryCurf == iLenf + nLengthOfTheSamePixelIntensities_ForObjectBoundary_Min;
					{
						nResf = CharacteristicsOfNeighborsInARow(
							sColor_Imagef, //const COLOR_IMAGE *sColor_Imagef, //[nImageSizeMax]

							iWidf, //const int nWidCurf,
							nLenOfObjectBoundaryCurf, //const int nLenCurf,
													  /////////////////////////////////////////////////////////////
							nNumOfBlackNeighboingPixelsFrTheSameRowf, //int &nNumOfBlackNeighboingPixelsFrTheSameRowf,
							nAverIntensityOverNeighboingPixelsFrTheSameRowf, //int &nAverIntensityOverNeighboingPixelsFrTheSameRowf,
							nDiffBetweenMaxAndMinf); // int &nDiffBetweenMaxAndMinf);

						fprintf(fout, "\n\n After 'CharacteristicsOfNeighborsInARow' 2: iWidf = %d, nLenOfObjectBoundaryCurf = %d, nNumOfBlackNeighboingPixelsFrTheSameRowf = %d",
							iWidf, nLenOfObjectBoundaryCurf, nNumOfBlackNeighboingPixelsFrTheSameRowf);
						fprintf(fout, "\n\n nAverIntensityOverNeighboingPixelsFrTheSameRowf = %d, nDiffBetweenMaxAndMinf = %d",
							nAverIntensityOverNeighboingPixelsFrTheSameRowf, nDiffBetweenMaxAndMinf);

						if (nNumOfBlackNeighboingPixelsFrTheSameRowf >= nNumOfBlackNeighboingPixelsFrTheSameRowMax)
						{
							fprintf(fout, "\n\n 2: discarding a possible left boundary by nNumOfBlackNeighboingPixelsFrTheSameRowf = %d >= nNumOfBlackNeighboingPixelsFrTheSameRowMax = %d",
								nNumOfBlackNeighboingPixelsFrTheSameRowf, nNumOfBlackNeighboingPixelsFrTheSameRowMax);

							fprintf(fout, "\n Current nLenOfObjectBoundaryCurf = %d, sColor_Imagef->nLenObjectBoundary_Arr[%d] = %d", nLenOfObjectBoundaryCurf, iWidf, sColor_Imagef->nLenObjectBoundary_Arr[iWidf]);

							nIs_ExitFromALinePossiblef = 0; // the exit from a line is impossible until 'nIs_ExitFromALinePossiblef = 1'
							continue;
							//break; // for (iLenf = sColor_Imagef->nLength - 1; iLenf >= 0; iLenf--)
						}//if (nNumOfBlackNeighboingPixelsFrTheSameRowf >= nNumOfBlackNeighboingPixelsFrTheSameRowMax)

						if (nAverIntensityOverNeighboingPixelsFrTheSameRowf <= nAverIntensityOverNeighboingPixelsFrTheSameRowMin)
						{
							fprintf(fout, "\n\n 2: discarding a possible left boundary by nAverIntensityOverNeighboingPixelsFrTheSameRowf = %d <= nAverIntensityOverNeighboingPixelsFrTheSameRowMin = %d",
								nAverIntensityOverNeighboingPixelsFrTheSameRowf, nAverIntensityOverNeighboingPixelsFrTheSameRowMin);

							fprintf(fout, "\n Current nLenOfObjectBoundaryCurf = %d, sColor_Imagef->nLenObjectBoundary_Arr[%d] = %d", nLenOfObjectBoundaryCurf, iWidf, sColor_Imagef->nLenObjectBoundary_Arr[iWidf]);

							nIs_ExitFromALinePossiblef = 0; // the exit from a line is impossible until 'nIs_ExitFromALinePossiblef = 1'
							continue;
							//break; // for (iLenf = sColor_Imagef->nLength - 1; iLenf >= 0; iLenf--)
						}//if (nAverIntensityOverNeighboingPixelsFrTheSameRowf <= nAverIntensityOverNeighboingPixelsFrTheSameRowMin)

					} //if (nLenOfObjectBoundaryCurf < sColor_Imagef->nLength - 1) 

					nNumOfLeftBoundariesf += 1;
					//////
					nNumOfNonZeroObjectBoundariesf += 1;
					sColor_Imagef->nLenObjectBoundary_Arr[iWidf] = nLenOfObjectBoundaryCurf;

					fprintf(fout, "\n\n The next possible left boundary of the object: sColor_Imagef->nLenObjectBoundary_Arr[%d] = %d, a new nNumOfLeftBoundariesf = %d, sColor_Imagef->nLength = %d",
						iWidf, sColor_Imagef->nLenObjectBoundary_Arr[iWidf], nNumOfLeftBoundariesf, sColor_Imagef->nLength);

					//if (iWidf == 288)
					if (iWidf == -1) // no printing
					{
						fprintf(fout, "\n nDiffOfLensf = %d, nDiffOfLensForBoundariesf = %d, nDistBetweenBoundariesOfNeighborWids_Max = %d, sColor_Imagef->nLenObjectBoundary_Arr[iWidf - 1] = %d",
							nDiffOfLensf, nDiffOfLensForBoundariesf, nDistBetweenBoundariesOfNeighborWids_Max, sColor_Imagef->nLenObjectBoundary_Arr[iWidf - 1]);
					} // if (iWidf == 288)

					if (nNumOfLeftBoundariesf == 1)
					{
						nLenOf_1stObjectBoundaryCurf = nLenOfObjectBoundaryCurf;
					} //if (nNumOfLeftBoundariesf == 1)
					else if (nNumOfLeftBoundariesf == 2)
					{
						nLenOf_2ndObjectBoundaryCurf = nLenOfObjectBoundaryCurf;
					} //if (nNumOfLeftBoundariesf == 2)

				} //if (iLenf < sColor_Imagef->nLength - 1 && nIs_ExitFromALinePossiblef == 1 && nWidForfNonZeroObjectBoundariesf != -1 && ...

				  //////////////
				if (iLenf == 0 && sColor_Imagef->nLenObjectBoundary_Arr[iWidf] == -1)
				{
					sColor_Imagef->nLenObjectBoundary_Arr[iWidf] = sColor_Imagef->nLength - 1;

					nNumOfLeftBoundariesf += 1;
					sColor_Imagef->nLenObjectBoundary_Arr[iWidf] = 0;

					fprintf(fout, "\n\n The left edge of the image: sColor_Imagef->nLenObjectBoundary_Arr[%d] = %d, nNumOfLeftBoundariesf = %d, sColor_Imagef->nLength = %d",
						iWidf, sColor_Imagef->nLenObjectBoundary_Arr[iWidf], nNumOfLeftBoundariesf, sColor_Imagef->nLength);

					fprintf(fout, "\n iWidf = %d, iLenf = %d", iWidf, iLenf);

					fprintf(fout, "\n\n nLengthOfTheSamePixelIntensities_ForObjectBoundary_Curf = %d, nLengthOfTheSamePixelIntensities_ForObjectBoundary_Min = %d",
						nLengthOfTheSamePixelIntensities_ForObjectBoundary_Curf, nLengthOfTheSamePixelIntensities_ForObjectBoundary_Min);

				} // if (iLenf == 0 && sColor_Imagef->nLenObjectBoundary_Arr[iWidf] == -1)

			} //for (iLenf = sColor_Imagef->nLength - 1; iLenf >= 0; iLenf--)

			if (nWidForfNonZeroObjectBoundariesf == -1)
			{
				sColor_Imagef->nLenObjectBoundary_Arr[iWidf] = sColor_Imagef->nLength - 1;

				fprintf(fout, "\n\n nWidForfNonZeroObjectBoundariesf == -1: sColor_Imagef->nLenObjectBoundary_Arr[%d] is adjusted to %d", iWidf, sColor_Imagef->nLenObjectBoundary_Arr[iWidf]);
			} //if (nWidForfNonZeroObjectBoundariesf == -1)

			  //nNumOfNonZeroObjectBoundariesf
			fprintf(fout, "\n\n Final current sColor_Imagef->nLenObjectBoundary_Arr[%d] = %d", iWidf, sColor_Imagef->nLenObjectBoundary_Arr[iWidf]);

		} //for (iWidf = 0; iWidf < sColor_Imagef->nWidth; iWidf++)

	} // if (sColor_Imagef->nSideOfObjectLocation == 1)

	return 1;
} //int DeterminingLenOfObjectBoundary(...
////////////////////////////////////////////////////////////////////////////////////

int CharacteristicsOfNeighborsInARow(

	const COLOR_IMAGE *sColor_Imagef, //[nImageSizeMax]

	const int nWidCurf,
	const int nLenCurf,
	/////////////////////////////////////////////////////////////
	int &nNumOfBlackNeighboingPixelsFrTheSameRowf,
	int &nAverIntensityOverNeighboingPixelsFrTheSameRowf,
	int &nDiffBetweenMaxAndMinf)
{
	int
		nResf,
		nIndexOfPixelCurf,

		//iWidf,
		iLenf,

		nLenMinf,
		nLenMaxf,

		nRedCurf,
		nGreenCurf,
		nBlueCurf,

		nRedAverf = 0,
		nGreenAverf = 0,
		nBlueAverf = 0,

		nAverOfRedGreenBluef = 0,
		nAverOfRedGreenBlueMinf = nLarge,
		nAverOfRedGreenBlueMaxf = -nLarge,

		nLenOfPixelSegmentOfOneRowf;

	float
		fRatioOfLengthsf;

	if (nWidCurf < 0 || nWidCurf > sColor_Imagef->nWidth - 1 || nLenCurf <= 0 || nLenCurf >= sColor_Imagef->nLength - 1)
	{
		printf("\n\n An error in 'CharacteristicsOfNeighborsInARow' 1: ...->nWidth - 1 = %d, nWidCurf = %d, ...->nLength - 1 = %d, nLenCurf = %d",
			sColor_Imagef->nWidth - 1, nWidCurf, sColor_Imagef->nLength - 1, nLenCurf);

		fprintf(fout, "\n\n An error in 'CharacteristicsOfNeighborsInARow' 1: ...->nWidth - 1 = %d, nWidCurf = %d, ...->nLength - 1 = %d, nLenCurf = %d",
			sColor_Imagef->nWidth - 1, nWidCurf, sColor_Imagef->nLength - 1, nLenCurf);

		printf("\n\n Please press any key to exit");
		getchar(); exit(1);
	} // if (nWidCurf < 0 || nWidCurf > sColor_Imagef->nWidth - 1 || nLenCurf <= 0 || nLenCurf >= sColor_Imagef->nLength - 1)

	fprintf(fout, "\n\n ////////////////////////////////////////////////////////////");
	fprintf(fout, "\n 'CharacteristicsOfNeighborsInARow' 1: ...->nWidth - 1 = %d, nWidCurf = %d, ...->nLength - 1 = %d, nLenCurf = %d, sColor_Imagef->nSideOfObjectLocation = %d",
		sColor_Imagef->nWidth - 1, nWidCurf, sColor_Imagef->nLength - 1, nLenCurf, sColor_Imagef->nSideOfObjectLocation);


	nNumOfBlackNeighboingPixelsFrTheSameRowf = 0;
	nAverIntensityOverNeighboingPixelsFrTheSameRowf = 0;
	nDiffBetweenMaxAndMinf = -nLarge;

	///////////////////////////////////////////////////////////////
	//object to the left   
	if (sColor_Imagef->nSideOfObjectLocation == -1)
	{
		//start counting from the left
		nLenMinf = nLenCurf - nLenOfNeighboringPixelsFrTheSameRow;
		if (nLenMinf < 0)
			nLenMinf = 0;

		nLenOfPixelSegmentOfOneRowf = nLenCurf - nLenMinf + 1;

		for (iLenf = nLenMinf; iLenf <= nLenCurf; iLenf++)
		{
			nIndexOfPixelCurf = iLenf + (nWidCurf*sColor_Imagef->nLength); // nLenMax);

			nRedCurf = sColor_Imagef->nRed_Arr[nIndexOfPixelCurf];
			nGreenCurf = sColor_Imagef->nGreen_Arr[nIndexOfPixelCurf];
			nBlueCurf = sColor_Imagef->nBlue_Arr[nIndexOfPixelCurf];

			nAverOfRedGreenBluef = (nRedCurf + nGreenCurf + nBlueCurf) / 3;

			if (nAverOfRedGreenBluef < nAverOfRedGreenBlueMinf)
			{
				nAverOfRedGreenBlueMinf = nAverOfRedGreenBluef;
			}// if (nAverOfRedGreenBluef < nAverOfRedGreenBlueMinf)

			if (nAverOfRedGreenBluef > nAverOfRedGreenBlueMaxf)
			{
				nAverOfRedGreenBlueMaxf = nAverOfRedGreenBluef;
			}// if (nAverOfRedGreenBluef > nAverOfRedGreenBlueMaxf)

			nRedAverf += nRedCurf;
			nGreenAverf += nGreenCurf;
			nBlueAverf += nBlueCurf;

			//if (nRedCurf == sColor_Imagef->nIntensityOfBackground_Red && nGreenCurf == sColor_Imagef->nIntensityOfBackground_Green && 
			//nBlueCurf == sColor_Imagef->nIntensityOfBackground_Blue)
			if (nRedCurf < nIntensityCloseToBlackMax && nGreenCurf < nIntensityCloseToBlackMax &&
				nBlueCurf < nIntensityCloseToBlackMax)
			{
				nNumOfBlackNeighboingPixelsFrTheSameRowf += 1;
			} // if (nRedCurf < nIntensityCloseToBlackMax && nGreenCurf < nIntensityCloseToBlackMax && ...

			if (nWidCurf == 9 && nLenCurf == 1)
			{
				fprintf(fout, "\n\n iLenf = %d, nLenCurf = %d, nWidCurf = %d, nLenOfPixelSegmentOfOneRowf = %d", iLenf, nLenCurf, nWidCurf, nLenOfPixelSegmentOfOneRowf);
				fprintf(fout, "\n nRedCurf = %d, nGreenCurf = %d, nBlueCurf = %d, nNumOfBlackNeighboingPixelsFrTheSameRowf = %d",
					nRedCurf, nGreenCurf, nBlueCurf, nNumOfBlackNeighboingPixelsFrTheSameRowf);

				fprintf(fout, "\n nRedAverf = %d, nGreenAverf = %d, nBlueAverf = %d", nRedAverf, nGreenAverf, nBlueAverf);

			}//if (nWidCurf == 9 && nLenCurf == 1)

		} //for (iLenf = nLenMinf; iLenf <= nLenCurf; iLenf++)

		nRedAverf = nRedAverf / nLenOfPixelSegmentOfOneRowf;
		nGreenAverf = nGreenAverf / nLenOfPixelSegmentOfOneRowf;
		nBlueAverf = nBlueAverf / nLenOfPixelSegmentOfOneRowf;

		nAverIntensityOverNeighboingPixelsFrTheSameRowf = (nRedAverf + nGreenAverf + nBlueAverf) / 3;

		nDiffBetweenMaxAndMinf = nAverOfRedGreenBlueMaxf - nAverOfRedGreenBlueMinf;

	} // if (sColor_Imagef->nSideOfObjectLocation == -1)

	  ////////////////////////////////////////////////////////////////////////////////////////////
	  ////object to the right
	if (sColor_Imagef->nSideOfObjectLocation == 1)
	{
		//still, start counting from the left
		nLenMaxf = nLenCurf + nLenOfNeighboringPixelsFrTheSameRow;

		if (nLenMaxf > sColor_Imagef->nLength - 1)
			nLenMaxf = sColor_Imagef->nLength - 1;

		nLenOfPixelSegmentOfOneRowf = nLenMaxf - nLenCurf + 1;

		for (iLenf = nLenCurf; iLenf <= nLenMaxf; iLenf++)
		{
			nIndexOfPixelCurf = iLenf + (nWidCurf*sColor_Imagef->nLength); // nLenMax);

			nRedCurf = sColor_Imagef->nRed_Arr[nIndexOfPixelCurf];
			nGreenCurf = sColor_Imagef->nGreen_Arr[nIndexOfPixelCurf];
			nBlueCurf = sColor_Imagef->nBlue_Arr[nIndexOfPixelCurf];

			nAverOfRedGreenBluef = (nRedCurf + nGreenCurf + nBlueCurf) / 3;

			if (nAverOfRedGreenBluef < nAverOfRedGreenBlueMinf)
			{
				nAverOfRedGreenBlueMinf = nAverOfRedGreenBluef;
			}// if (nAverOfRedGreenBluef < nAverOfRedGreenBlueMinf)

			if (nAverOfRedGreenBluef > nAverOfRedGreenBlueMaxf)
			{
				nAverOfRedGreenBlueMaxf = nAverOfRedGreenBluef;
			}// if (nAverOfRedGreenBluef > nAverOfRedGreenBlueMaxf)

			nRedAverf += nRedCurf;
			nGreenAverf += nGreenCurf;
			nBlueAverf += nBlueCurf;

			if (nRedCurf < nIntensityCloseToBlackMax && nGreenCurf < nIntensityCloseToBlackMax &&
				nBlueCurf < nIntensityCloseToBlackMax)
			{
				nNumOfBlackNeighboingPixelsFrTheSameRowf += 1;
			} // if (nRedCurf < nIntensityCloseToBlackMax && nGreenCurf < nIntensityCloseToBlackMax && ...

		} //for (iLenf = nLenCurf; iLenf <= nLenMaxf; iLenf++)

		nRedAverf = nRedAverf / nLenOfPixelSegmentOfOneRowf;
		nGreenAverf = nGreenAverf / nLenOfPixelSegmentOfOneRowf;
		nBlueAverf = nBlueAverf / nLenOfPixelSegmentOfOneRowf;

		nAverIntensityOverNeighboingPixelsFrTheSameRowf = (nRedAverf + nGreenAverf + nBlueAverf) / 3;

		nDiffBetweenMaxAndMinf = nAverOfRedGreenBlueMaxf - nAverOfRedGreenBlueMinf;
	} // if (sColor_Imagef->nSideOfObjectLocation == 1)

	  //scaling the number of black pixels in case of closeness to the edge
	if (nLenOfPixelSegmentOfOneRowf < nLenOfNeighboringPixelsFrTheSameRow)
	{
		fRatioOfLengthsf = (float)(nLenOfNeighboringPixelsFrTheSameRow) / (float)(nLenOfPixelSegmentOfOneRowf);

		nNumOfBlackNeighboingPixelsFrTheSameRowf = (int)((float)(nNumOfBlackNeighboingPixelsFrTheSameRowf)*fRatioOfLengthsf);
	} //if (nLenOfPixelSegmentOfOneRowf < nLenOfNeighboringPixelsFrTheSameRow)

	return 1;
} //int CharacteristicsOfNeighborsInARow(...
////////////////////////////////////////////////////////////////////////////////////
int Erasing_Labels(

	COLOR_IMAGE *sColor_Imagef) //[nImageSizeMax]
{
	int
		nResf,
		nIndexOfPixelCurf,

		iWidf,
		iLenf,

		nRedCurf,
		nGreenCurf,
		nBlueCurf,

		nLenOfObjectBoundaryCurf;
	
	//object to the left   
	if (sColor_Imagef->nSideOfObjectLocation == -1)
	{
		for (iWidf = 0; iWidf < sColor_Imagef->nWidth; iWidf++)
		{
//start counting from the right

			nLenOfObjectBoundaryCurf = sColor_Imagef->nLenObjectBoundary_Arr[iWidf];
			if (nLenOfObjectBoundaryCurf < 0 || nLenOfObjectBoundaryCurf > sColor_Imagef->nLength - 1)
			{
				if (nLenOfObjectBoundaryCurf == -1)
				{
					fprintf(fout, "\n\n No boundary/erasing in 'Erasing_Labels' 1: nLenOfObjectBoundaryCurf = %d, sColor_Imagef->nLength - 1 = %d, iWidf = %d",
						nLenOfObjectBoundaryCurf, sColor_Imagef->nLength - 1, iWidf);

					continue;
				} //if (nLenOfObjectBoundaryCurf == -1)

				fprintf(fout,"\n\n An error in 'Erasing_Labels' 1: nLenOfObjectBoundaryCurf = %d, sColor_Imagef->nLength - 1 = %d, iWidf = %d",
					nLenOfObjectBoundaryCurf, sColor_Imagef->nLength - 1, iWidf);
				printf("\n\n An error in 'Erasing_Labels' 1: nLenOfObjectBoundaryCurf = %d, sColor_Imagef->nLength - 1 = %d, iWidf = %d",
					nLenOfObjectBoundaryCurf, sColor_Imagef->nLength - 1, iWidf);

				return (-1);
				//printf("\n\n Please press any key to exit");
				//getchar(); exit(1);
			}// if (nLenOfObjectBoundaryCurf < 0 || nLenOfObjectBoundaryCurf > sColor_Imagef->nLength - 1)

			//for (iLenf = 0; iLenf < sColor_Imagef->nLength; iLenf++)
			for (iLenf = sColor_Imagef->nLength - 1; iLenf > nLenOfObjectBoundaryCurf; iLenf--)
			{
				nIndexOfPixelCurf = iLenf + (iWidf*sColor_Imagef->nLength); // nLenMax);

				nRedCurf = sColor_Imagef->nRed_Arr[nIndexOfPixelCurf];
				nGreenCurf = sColor_Imagef->nGreen_Arr[nIndexOfPixelCurf];
				nBlueCurf = sColor_Imagef->nBlue_Arr[nIndexOfPixelCurf];

				if (nRedCurf != sColor_Imagef->nIntensityOfBackground_Red)
				{
					sColor_Imagef->nRed_Arr[nIndexOfPixelCurf] = sColor_Imagef->nIntensityOfBackground_Red;
				} // if (nRedCurf != sColor_Imagef->nIntensityOfBackground_Red)

				if (nGreenCurf != sColor_Imagef->nIntensityOfBackground_Green)
				{
					sColor_Imagef->nGreen_Arr[nIndexOfPixelCurf] = sColor_Imagef->nIntensityOfBackground_Green;
				} // if (nGreenCurf != sColor_Imagef->nIntensityOfBackground_Green)

				if (nBlueCurf != sColor_Imagef->nIntensityOfBackground_Blue)
				{
					sColor_Imagef->nBlue_Arr[nIndexOfPixelCurf] = sColor_Imagef->nIntensityOfBackground_Blue;
				} // if (nBlueCurf != sColor_Imagef->nIntensityOfBackground_Blue)

			} //for (iLenf = sColor_Imagef->nLength - 1; iLenf > nLenOfObjectBoundaryCurf; iLenf--)

		} //for (iWidf = 0; iWidf < sColor_Imagef->nWidth; iWidf++)
	} // if (sColor_Imagef->nSideOfObjectLocation == -1)

	 ////object to the right
	if (sColor_Imagef->nSideOfObjectLocation == 1)
	{
		for (iWidf = 0; iWidf < sColor_Imagef->nWidth; iWidf++)
		{

//start counting from the left

			nLenOfObjectBoundaryCurf = sColor_Imagef->nLenObjectBoundary_Arr[iWidf];
			if (nLenOfObjectBoundaryCurf < 0 || nLenOfObjectBoundaryCurf > sColor_Imagef->nLength - 1)
			{
				if (nLenOfObjectBoundaryCurf == -1)
				{
					fprintf(fout, "\n\n No boundary/erasing in 'Erasing_Labels' 2: nLenOfObjectBoundaryCurf = %d, sColor_Imagef->nLength - 1 = %d, iWidf = %d",
						nLenOfObjectBoundaryCurf, sColor_Imagef->nLength - 1, iWidf);

					continue;
				} //if (nLenOfObjectBoundaryCurf == -1)

				fprintf(fout, "\n\n An error in 'Erasing_Labels' 2: nLenOfObjectBoundaryCurf = %d, sColor_Imagef->nLength - 1 = %d, iWidf = %d",
					nLenOfObjectBoundaryCurf, sColor_Imagef->nLength - 1, iWidf);
				printf("\n\n An error in 'Erasing_Labels' 2: nLenOfObjectBoundaryCurf = %d, sColor_Imagef->nLength - 1 = %d, iWidf = %d",
					nLenOfObjectBoundaryCurf, sColor_Imagef->nLength - 1, iWidf);

				return (-1);
				//printf("\n\n Please press any key to exit");
				//getchar(); exit(1);
			}// if (nLenOfObjectBoundaryCurf < 0 || nLenOfObjectBoundaryCurf > sColor_Imagef->nLength - 1)

			for (iLenf = 0; iLenf < nLenOfObjectBoundaryCurf; iLenf++)
			{
				nIndexOfPixelCurf = iLenf + (iWidf*sColor_Imagef->nLength); // nLenMax);

				nRedCurf = sColor_Imagef->nRed_Arr[nIndexOfPixelCurf];
				nGreenCurf = sColor_Imagef->nGreen_Arr[nIndexOfPixelCurf];
				nBlueCurf = sColor_Imagef->nBlue_Arr[nIndexOfPixelCurf];

				if (nRedCurf != sColor_Imagef->nIntensityOfBackground_Red)
				{
					sColor_Imagef->nRed_Arr[nIndexOfPixelCurf] = sColor_Imagef->nIntensityOfBackground_Red;
				} // if (nRedCurf != sColor_Imagef->nIntensityOfBackground_Red)

				if (nGreenCurf != sColor_Imagef->nIntensityOfBackground_Green)
				{
					sColor_Imagef->nGreen_Arr[nIndexOfPixelCurf] = sColor_Imagef->nIntensityOfBackground_Green;
				} // if (nGreenCurf != sColor_Imagef->nIntensityOfBackground_Green)

				if (nBlueCurf != sColor_Imagef->nIntensityOfBackground_Blue)
				{
					sColor_Imagef->nBlue_Arr[nIndexOfPixelCurf] = sColor_Imagef->nIntensityOfBackground_Blue;
				} // if (nBlueCurf != sColor_Imagef->nIntensityOfBackground_Blue)

			} //for (iLenf = 0; iLenf < nLenOfObjectBoundaryCurf; iLenf++)

		} //for (iWidf = 0; iWidf < sColor_Imagef->nWidth; iWidf++)
	} // if (sColor_Imagef->nSideOfObjectLocation == 1)

	return 1;
} //int Erasing_Labels(...

  //////////////////////////////////////////////////////////////////
int Detecting_BackgroundPixels(

	COLOR_IMAGE *sColor_Imagef) //[nImageSizeMax]
{
	int
		nResf,
		nIndexOfPixelCurf,

		nIndexOfPixelNeighborToRightf,

		nIndexOfPixelNeighborToRightBelowf,
		nIndexOfPixelNeighborToBelowf,
		nIndexOfPixelNeighborToLeftBelowf,

		nIndexOfPixelNeighborToRightAbovef,
		nIndexOfPixelNeighborToAbovef,
		nIndexOfPixelNeighborToLeftAbovef,

		iWidf,
		iLenf,

		nRedCurf,
		nGreenCurf,
		nBlueCurf,

		nNumOfBlackPixelsInTheLastRowMinf = fLowestPercentageOfBlackPixelsInTheLastRow*sColor_Imagef->nLength,
		nNumOfTheLastRowsWithInsufficientNumberOfBlackPixelsMaxf = fLargestPercentageOfTheLastRowsWithInsufficientNumberOfBlackPixels*sColor_Imagef->nWidth,

		nIsAPixel_BackgroundOnef,

		nBottomOfBlackPixelsf = -1,
		nTopOfBlackPixelsf = -1,

		nNumOfBlackPixelsInALineTotf,

		nWidFor1stRowWithBlackPixelsf,

		nLenOfObjectBoundaryCurf;

	//int *nLeftBoundaryOfBlackPixelsArrf = new int[sColor_Imagef->nWidth];
	//int *nRightBoundaryOfBlackPixelsArrf = new int[sColor_Imagef->nWidth];

	nWidFor1stRowWithBlackPixelsf = sColor_Imagef->nWidth - 1;

	//object to the left   
	if (sColor_Imagef->nSideOfObjectLocation == -1)
	{
		////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		//from Up to Down, the object is to the left 

		for (iWidf = sColor_Imagef->nWidth - 1; iWidf >= 0; iWidf--)
		{
			nLenOfObjectBoundaryCurf = sColor_Imagef->nLenObjectBoundary_Arr[iWidf];

			if (nLenOfObjectBoundaryCurf < 0 || nLenOfObjectBoundaryCurf > sColor_Imagef->nLength - 1)
			{
				printf("\n\n An error in 'Detecting_BackgroundPixels' Up to Down: nLenOfObjectBoundaryCurf = %d, sColor_Imagef->nLength - 1 = %d, iWidf = %d",
					nLenOfObjectBoundaryCurf, sColor_Imagef->nLength - 1, iWidf);

				fprintf(fout, "\n\n An error in 'Detecting_BackgroundPixels' Up to Down: nLenOfObjectBoundaryCurf = %d, sColor_Imagef->nLength - 1 = %d, iWidf = %d",
					nLenOfObjectBoundaryCurf, sColor_Imagef->nLength - 1, iWidf);

				printf("\n\n Please press any key to exit");
				getchar(); exit(1);
			}// if (nLenOfObjectBoundaryCurf < 0 || nLenOfObjectBoundaryCurf > sColor_Imagef->nLength - 1)

			 //start counting from the right for the left object
			nNumOfBlackPixelsInALineTotf = 0;

			//for (iLenf = sColor_Imagef->nLength - 1; iLenf >= nLenOfObjectBoundaryCurf; iLenf--)
			for (iLenf = sColor_Imagef->nLength - 1; iLenf >= 0; iLenf--)
			{
				nIndexOfPixelCurf = iLenf + (iWidf*sColor_Imagef->nLength); // nLenMax);

				nRedCurf = sColor_Imagef->nRed_Arr[nIndexOfPixelCurf];
				nGreenCurf = sColor_Imagef->nGreen_Arr[nIndexOfPixelCurf];
				nBlueCurf = sColor_Imagef->nBlue_Arr[nIndexOfPixelCurf];

				if (iWidf == nWidFor1stRowWithBlackPixelsf) //sColor_Imagef->nWidth - 1)
				{
					if (nRedCurf == sColor_Imagef->nIntensityOfBackground_Red && nGreenCurf == sColor_Imagef->nIntensityOfBackground_Green &&
						nBlueCurf == sColor_Imagef->nIntensityOfBackground_Blue)
					{
						nNumOfBlackPixelsInALineTotf += 1;
						sColor_Imagef->nIsAPixelBackground_Arr[nIndexOfPixelCurf] = 1; // the pixel belongs to the background
						continue;
					} // if (nRedCurf == sColor_Imagef->nIntensityOfBackground_Red && nGreenCurf == sColor_Imagef->nIntensityOfBackground_Green && ...

				}//if (iWidf == nWidFor1stRowWithBlackPixelsf)
				else // if (iWidf < sColor_Imagef->nWidth - 1)
				{

					if (iLenf == sColor_Imagef->nLength - 1)
					{
						// No neighbor to the left or left below

						/////////////////////////////////////////////////////
						nIndexOfPixelNeighborToBelowf = iLenf + ((iWidf + 1)*sColor_Imagef->nLength); // a neighbor to the exactly below
						nIsAPixel_BackgroundOnef = sColor_Imagef->nIsAPixelBackground_Arr[nIndexOfPixelNeighborToBelowf];

						if (nIsAPixel_BackgroundOnef == 1)
						{

							if (nRedCurf == sColor_Imagef->nIntensityOfBackground_Red && nGreenCurf == sColor_Imagef->nIntensityOfBackground_Green &&
								nBlueCurf == sColor_Imagef->nIntensityOfBackground_Blue)
							{
								nNumOfBlackPixelsInALineTotf += 1;
								sColor_Imagef->nIsAPixelBackground_Arr[nIndexOfPixelCurf] = 1; // the pixel belongs to the background

							} // if (nRedCurf == sColor_Imagef->nIntensityOfBackground_Red && nGreenCurf == sColor_Imagef->nIntensityOfBackground_Green && ...
							continue;
						} //if (nIsAPixel_BackgroundOnef == 1)

						  /////////////////////////////////////////////////////

						nIndexOfPixelNeighborToLeftBelowf = iLenf - 1 + ((iWidf + 1)*sColor_Imagef->nLength); // a neighbor to the left below
						nIsAPixel_BackgroundOnef = sColor_Imagef->nIsAPixelBackground_Arr[nIndexOfPixelNeighborToLeftBelowf];

						if (nIsAPixel_BackgroundOnef == 1)
						{
							if (nRedCurf == sColor_Imagef->nIntensityOfBackground_Red && nGreenCurf == sColor_Imagef->nIntensityOfBackground_Green &&
								nBlueCurf == sColor_Imagef->nIntensityOfBackground_Blue)
							{
								nNumOfBlackPixelsInALineTotf += 1;
								sColor_Imagef->nIsAPixelBackground_Arr[nIndexOfPixelCurf] = 1; // the pixel belongs to the background

							} // if (nRedCurf == sColor_Imagef->nIntensityOfBackground_Red && nGreenCurf == sColor_Imagef->nIntensityOfBackground_Green && ...
							continue;
						} //if (nIsAPixel_BackgroundOnef == 1)

					} //if (iLenf == sColor_Imagef->nLength - 1)
					  //////////////////////////////////////////////////////////
					if (iLenf < sColor_Imagef->nLength - 1)
					{
						nIndexOfPixelNeighborToRightf = iLenf + 1 + (iWidf*sColor_Imagef->nLength); // a neighbor to the right

						nIsAPixel_BackgroundOnef = sColor_Imagef->nIsAPixelBackground_Arr[nIndexOfPixelNeighborToRightf];

						if (nIsAPixel_BackgroundOnef == 1)
						{

							if (nRedCurf == sColor_Imagef->nIntensityOfBackground_Red && nGreenCurf == sColor_Imagef->nIntensityOfBackground_Green &&
								nBlueCurf == sColor_Imagef->nIntensityOfBackground_Blue)
							{
								nNumOfBlackPixelsInALineTotf += 1;
								sColor_Imagef->nIsAPixelBackground_Arr[nIndexOfPixelCurf] = 1; // the pixel belongs to the background

							} // if (nRedCurf == sColor_Imagef->nIntensityOfBackground_Red && nGreenCurf == sColor_Imagef->nIntensityOfBackground_Green && ...
							continue;
						} //if (nIsAPixel_BackgroundOnef == 1)
						  /////////////////////////////////////////////////////

						nIndexOfPixelNeighborToRightBelowf = iLenf + 1 + ((iWidf + 1)*sColor_Imagef->nLength); // a neighbor to the right below
						nIsAPixel_BackgroundOnef = sColor_Imagef->nIsAPixelBackground_Arr[nIndexOfPixelNeighborToRightBelowf];

						if (nIsAPixel_BackgroundOnef == 1)
						{

							if (nRedCurf == sColor_Imagef->nIntensityOfBackground_Red && nGreenCurf == sColor_Imagef->nIntensityOfBackground_Green &&
								nBlueCurf == sColor_Imagef->nIntensityOfBackground_Blue)
							{
								nNumOfBlackPixelsInALineTotf += 1;
								sColor_Imagef->nIsAPixelBackground_Arr[nIndexOfPixelCurf] = 1; // the pixel belongs to the background

							} // if (nRedCurf == sColor_Imagef->nIntensityOfBackground_Red && nGreenCurf == sColor_Imagef->nIntensityOfBackground_Green && ...
							continue;
						} //if (nIsAPixel_BackgroundOnef == 1)

						  /////////////////////////////////////////////////////

						nIndexOfPixelNeighborToBelowf = iLenf + ((iWidf + 1)*sColor_Imagef->nLength); // a neighbor to the exactly below
						nIsAPixel_BackgroundOnef = sColor_Imagef->nIsAPixelBackground_Arr[nIndexOfPixelNeighborToBelowf];

						if (nIsAPixel_BackgroundOnef == 1)
						{

							if (nRedCurf == sColor_Imagef->nIntensityOfBackground_Red && nGreenCurf == sColor_Imagef->nIntensityOfBackground_Green &&
								nBlueCurf == sColor_Imagef->nIntensityOfBackground_Blue)
							{
								nNumOfBlackPixelsInALineTotf += 1;
								sColor_Imagef->nIsAPixelBackground_Arr[nIndexOfPixelCurf] = 1; // the pixel belongs to the background

							} // if (nRedCurf == sColor_Imagef->nIntensityOfBackground_Red && nGreenCurf == sColor_Imagef->nIntensityOfBackground_Green && ...
							continue;
						} //if (nIsAPixel_BackgroundOnef == 1)

						  /////////////////////////////////////////////////////
						nIndexOfPixelNeighborToLeftBelowf = iLenf - 1 + ((iWidf + 1)*sColor_Imagef->nLength); // a neighbor to the left below
						nIsAPixel_BackgroundOnef = sColor_Imagef->nIsAPixelBackground_Arr[nIndexOfPixelNeighborToLeftBelowf];

						if (nIsAPixel_BackgroundOnef == 1)
						{
							if (nRedCurf == sColor_Imagef->nIntensityOfBackground_Red && nGreenCurf == sColor_Imagef->nIntensityOfBackground_Green &&
								nBlueCurf == sColor_Imagef->nIntensityOfBackground_Blue)
							{
								nNumOfBlackPixelsInALineTotf += 1;
								sColor_Imagef->nIsAPixelBackground_Arr[nIndexOfPixelCurf] = 1; // the pixel belongs to the background

							} // if (nRedCurf == sColor_Imagef->nIntensityOfBackground_Red && nGreenCurf == sColor_Imagef->nIntensityOfBackground_Green && ...
							continue;
						} //if (nIsAPixel_BackgroundOnef == 1)

					} //if (iLenf < sColor_Imagef->nLength - 1)

				} // else // if (iWidf < sColor_Imagef->nWidth - 1)

			} //for (iLenf = sColor_Imagef->nLength - 1; iLenf >= 0; iLenf--)

			  //nNumOfBlackPixelsInTheLastRowMinf
			if (iWidf == nWidFor1stRowWithBlackPixelsf) //sColor_Imagef->nWidth - 1)
			{
				if (nNumOfBlackPixelsInALineTotf < nNumOfBlackPixelsInTheLastRowMinf)
				{
					nWidFor1stRowWithBlackPixelsf = nWidFor1stRowWithBlackPixelsf - 1;

					if (sColor_Imagef->nWidth - nWidFor1stRowWithBlackPixelsf > nNumOfTheLastRowsWithInsufficientNumberOfBlackPixelsMaxf)
					{
						printf("\n\n An error in 'Detecting_BackgroundPixels' from Up to Down: sColor_Imagef->nWidth - nWidFor1stRowWithBlackPixelsf = %d > nNumOfTheLastRowsWithInsufficientNumberOfBlackPixelsMaxf = %d",
							sColor_Imagef->nWidth - nWidFor1stRowWithBlackPixelsf, nNumOfTheLastRowsWithInsufficientNumberOfBlackPixelsMaxf);

						printf("\n nNumOfBlackPixelsInALineTotf = %d, nLenOfObjectBoundaryCurf = %d, sColor_Imagef->nLength - 1 = %d, iWidf = %d",
							nNumOfBlackPixelsInALineTotf, nLenOfObjectBoundaryCurf, sColor_Imagef->nLength - 1, iWidf);

						fprintf(fout, "\n\n An error in 'Detecting_BackgroundPixels' from Up to Down: sColor_Imagef->nWidth - nWidFor1stRowWithBlackPixelsf = %d > nNumOfTheLastRowsWithInsufficientNumberOfBlackPixelsMaxf = %d",
							sColor_Imagef->nWidth - nWidFor1stRowWithBlackPixelsf, nNumOfTheLastRowsWithInsufficientNumberOfBlackPixelsMaxf);

						fprintf(fout, "\n nNumOfBlackPixelsInALineTotf = %d, nLenOfObjectBoundaryCurf = %d, sColor_Imagef->nLength - 1 = %d, iWidf = %d",
							nNumOfBlackPixelsInALineTotf, nLenOfObjectBoundaryCurf, sColor_Imagef->nLength - 1, iWidf);

						printf("\n\n Please press any key to exit");
						getchar(); exit(1);

					} //if (sColor_Imagef->nWidth - nWidFor1stRowWithBlackPixelsf > nNumOfTheLastRowsWithInsufficientNumberOfBlackPixelsMaxf)

					continue; // for (iWidf = sColor_Imagef->nWidth - 1; iWidf >= 0; iWidf--)
				} //if (nNumOfBlackPixelsInALineTotf < nNumOfBlackPixelsInTheLastRowMinf) 

			}//if (iWidf == nWidFor1stRowWithBlackPixelsf)

			if (nNumOfBlackPixelsInALineTotf == 0)
			{
				printf("\n\n An error in 'Detecting_BackgroundPixels' from Up to Down: nNumOfBlackPixelsInALineTotf == 0, nLenOfObjectBoundaryCurf = %d, sColor_Imagef->nLength - 1 = %d, iWidf = %d",
					nLenOfObjectBoundaryCurf, sColor_Imagef->nLength - 1, iWidf);

				fprintf(fout, "\n\n An error in 'Detecting_BackgroundPixels' from Up to Down: nNumOfBlackPixelsInALineTotf == 0, nLenOfObjectBoundaryCurf = %d, sColor_Imagef->nLength - 1 = %d, iWidf = %d",
					nLenOfObjectBoundaryCurf, sColor_Imagef->nLength - 1, iWidf);

				printf("\n\n Please press any key to exit");
				getchar(); exit(1);
			} // if (nNumOfBlackPixelsInALineTotf == 0)

		} //for (iWidf = sColor_Imagef->nWidth - 1; iWidf >= 0; iWidf--)
		  //The end of Up to Down, the object is to the left

		  ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		  //from Down to Up, the object is to the left 
		  //for (iWidf = sColor_Imagef->nWidth - 1; iWidf >= 0; iWidf--)
		for (iWidf = 0; iWidf < sColor_Imagef->nWidth; iWidf++)
		{
			nLenOfObjectBoundaryCurf = sColor_Imagef->nLenObjectBoundary_Arr[iWidf];

			//start counting from the right for the left object
			nNumOfBlackPixelsInALineTotf = 0;

			//for (iLenf = sColor_Imagef->nLength - 1; iLenf >= nLenOfObjectBoundaryCurf; iLenf--)
			for (iLenf = sColor_Imagef->nLength - 1; iLenf >= 0; iLenf--)
			{
				nIndexOfPixelCurf = iLenf + (iWidf*sColor_Imagef->nLength); // nLenMax);

				if (sColor_Imagef->nIsAPixelBackground_Arr[nIndexOfPixelCurf] == 1)
				{
					nNumOfBlackPixelsInALineTotf += 1;
					continue;
				}// if (sColor_Imagef->nIsAPixelBackground_Arr[nIndexOfPixelCurf] == 1)

				nRedCurf = sColor_Imagef->nRed_Arr[nIndexOfPixelCurf];
				nGreenCurf = sColor_Imagef->nGreen_Arr[nIndexOfPixelCurf];
				nBlueCurf = sColor_Imagef->nBlue_Arr[nIndexOfPixelCurf];

				if (iWidf == 0)
				{
					if (nRedCurf == sColor_Imagef->nIntensityOfBackground_Red && nGreenCurf == sColor_Imagef->nIntensityOfBackground_Green &&
						nBlueCurf == sColor_Imagef->nIntensityOfBackground_Blue)
					{
						nNumOfBlackPixelsInALineTotf += 1;
						sColor_Imagef->nIsAPixelBackground_Arr[nIndexOfPixelCurf] = 1; // the pixel belongs to the background
						continue;
					} // if (nRedCurf == sColor_Imagef->nIntensityOfBackground_Red && nGreenCurf == sColor_Imagef->nIntensityOfBackground_Green && ...
				}//if (iWidf == 0)

				else // if (iWidf > 0)
				{

					if (iLenf == sColor_Imagef->nLength - 1)
					{
						// No neighbor to the right or right Above

						/////////////////////////////////////////////////////
						nIndexOfPixelNeighborToAbovef = iLenf + ((iWidf - 1)*sColor_Imagef->nLength); // a neighbor to the exactly Above
						nIsAPixel_BackgroundOnef = sColor_Imagef->nIsAPixelBackground_Arr[nIndexOfPixelNeighborToAbovef];

						if (nIsAPixel_BackgroundOnef == 1)
						{

							if (nRedCurf == sColor_Imagef->nIntensityOfBackground_Red && nGreenCurf == sColor_Imagef->nIntensityOfBackground_Green &&
								nBlueCurf == sColor_Imagef->nIntensityOfBackground_Blue)
							{
								nNumOfBlackPixelsInALineTotf += 1;
								sColor_Imagef->nIsAPixelBackground_Arr[nIndexOfPixelCurf] = 1; // the pixel belongs to the background

							} // if (nRedCurf == sColor_Imagef->nIntensityOfBackground_Red && nGreenCurf == sColor_Imagef->nIntensityOfBackground_Green && ...
							continue;
						} //if (nIsAPixel_BackgroundOnef == 1)

						  /////////////////////////////////////////////////////

						nIndexOfPixelNeighborToLeftAbovef = iLenf - 1 + ((iWidf - 1)*sColor_Imagef->nLength); // a neighbor to the left Above
						nIsAPixel_BackgroundOnef = sColor_Imagef->nIsAPixelBackground_Arr[nIndexOfPixelNeighborToLeftAbovef];

						if (nIsAPixel_BackgroundOnef == 1)
						{

							if (nRedCurf == sColor_Imagef->nIntensityOfBackground_Red && nGreenCurf == sColor_Imagef->nIntensityOfBackground_Green &&
								nBlueCurf == sColor_Imagef->nIntensityOfBackground_Blue)
							{
								nNumOfBlackPixelsInALineTotf += 1;
								sColor_Imagef->nIsAPixelBackground_Arr[nIndexOfPixelCurf] = 1; // the pixel belongs to the background

							} // if (nRedCurf == sColor_Imagef->nIntensityOfBackground_Red && nGreenCurf == sColor_Imagef->nIntensityOfBackground_Green && ...
							continue;
						} //if (nIsAPixel_BackgroundOnef == 1)

					} //if (iLenf == sColor_Imagef->nLength - 1)
					  //////////////////////////////////////////////////////////
					if (iLenf < sColor_Imagef->nLength - 1)
					{
						nIndexOfPixelNeighborToRightf = iLenf + 1 + (iWidf*sColor_Imagef->nLength); // a neighbor to the right

						nIsAPixel_BackgroundOnef = sColor_Imagef->nIsAPixelBackground_Arr[nIndexOfPixelNeighborToRightf];

						if (nIsAPixel_BackgroundOnef == 1)
						{

							if (nRedCurf == sColor_Imagef->nIntensityOfBackground_Red && nGreenCurf == sColor_Imagef->nIntensityOfBackground_Green &&
								nBlueCurf == sColor_Imagef->nIntensityOfBackground_Blue)
							{
								nNumOfBlackPixelsInALineTotf += 1;
								sColor_Imagef->nIsAPixelBackground_Arr[nIndexOfPixelCurf] = 1; // the pixel belongs to the background

							} // if (nRedCurf == sColor_Imagef->nIntensityOfBackground_Red && nGreenCurf == sColor_Imagef->nIntensityOfBackground_Green && ...
							continue;
						} //if (nIsAPixel_BackgroundOnef == 1)
						  /////////////////////////////////////////////////////

						nIndexOfPixelNeighborToRightAbovef = iLenf + 1 + ((iWidf - 1)*sColor_Imagef->nLength); // a neighbor to the right Above
						nIsAPixel_BackgroundOnef = sColor_Imagef->nIsAPixelBackground_Arr[nIndexOfPixelNeighborToRightAbovef];

						if (nIsAPixel_BackgroundOnef == 1)
						{

							if (nRedCurf == sColor_Imagef->nIntensityOfBackground_Red && nGreenCurf == sColor_Imagef->nIntensityOfBackground_Green &&
								nBlueCurf == sColor_Imagef->nIntensityOfBackground_Blue)
							{
								nNumOfBlackPixelsInALineTotf += 1;
								sColor_Imagef->nIsAPixelBackground_Arr[nIndexOfPixelCurf] = 1; // the pixel belongs to the background

							} // if (nRedCurf == sColor_Imagef->nIntensityOfBackground_Red && nGreenCurf == sColor_Imagef->nIntensityOfBackground_Green && ...
							continue;
						} //if (nIsAPixel_BackgroundOnef == 1)

						  /////////////////////////////////////////////////////

						nIndexOfPixelNeighborToAbovef = iLenf + ((iWidf - 1)*sColor_Imagef->nLength); // a neighbor to the exactly Above
						nIsAPixel_BackgroundOnef = sColor_Imagef->nIsAPixelBackground_Arr[nIndexOfPixelNeighborToAbovef];

						if (nIsAPixel_BackgroundOnef == 1)
						{

							if (nRedCurf == sColor_Imagef->nIntensityOfBackground_Red && nGreenCurf == sColor_Imagef->nIntensityOfBackground_Green &&
								nBlueCurf == sColor_Imagef->nIntensityOfBackground_Blue)
							{
								nNumOfBlackPixelsInALineTotf += 1;
								sColor_Imagef->nIsAPixelBackground_Arr[nIndexOfPixelCurf] = 1; // the pixel belongs to the background

							} // if (nRedCurf == sColor_Imagef->nIntensityOfBackground_Red && nGreenCurf == sColor_Imagef->nIntensityOfBackground_Green && ...
							continue;
						} //if (nIsAPixel_BackgroundOnef == 1)

						  /////////////////////////////////////////////////////
						nIndexOfPixelNeighborToLeftAbovef = iLenf - 1 + ((iWidf - 1)*sColor_Imagef->nLength); // a neighbor to the left Above
						nIsAPixel_BackgroundOnef = sColor_Imagef->nIsAPixelBackground_Arr[nIndexOfPixelNeighborToLeftAbovef];

						if (nIsAPixel_BackgroundOnef == 1)
						{
							if (nRedCurf == sColor_Imagef->nIntensityOfBackground_Red && nGreenCurf == sColor_Imagef->nIntensityOfBackground_Green &&
								nBlueCurf == sColor_Imagef->nIntensityOfBackground_Blue)
							{
								nNumOfBlackPixelsInALineTotf += 1;
								sColor_Imagef->nIsAPixelBackground_Arr[nIndexOfPixelCurf] = 1; // the pixel belongs to the background

							} // if (nRedCurf == sColor_Imagef->nIntensityOfBackground_Red && nGreenCurf == sColor_Imagef->nIntensityOfBackground_Green && ...
							continue;
						} //if (nIsAPixel_BackgroundOnef == 1)

					} //if (iLenf < sColor_Imagef->nLength - 1)

				} // else // if (iWidf < sColor_Imagef->nWidth - 1)

			} //for (iLenf = sColor_Imagef->nLength - 1; iLenf >= 0; iLenf--)

		} //for (iWidf = 0; iWidf < sColor_Imagef->nWidth; iWidf++)
		  //The end of Down to Up, the object is to the left

	} // if (sColor_Imagef->nSideOfObjectLocation == -1)
	  //////////////////////////////////////////////////////////////////////////////////////////////////////////

	  //object to the right
	if (sColor_Imagef->nSideOfObjectLocation == 1)
	{
		////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		//from Up to Down, the object is to the right 
		for (iWidf = sColor_Imagef->nWidth - 1; iWidf >= 0; iWidf--)
		{
			nLenOfObjectBoundaryCurf = sColor_Imagef->nLenObjectBoundary_Arr[iWidf];

			if (nLenOfObjectBoundaryCurf < 0 || nLenOfObjectBoundaryCurf > sColor_Imagef->nLength - 1)
			{
				printf("\n\n An error in 'Detecting_BackgroundPixels' Up to Down: nLenOfObjectBoundaryCurf = %d, sColor_Imagef->nLength - 1 = %d, iWidf = %d",
					nLenOfObjectBoundaryCurf, sColor_Imagef->nLength - 1, iWidf);

				fprintf(fout, "\n\n An error in 'Detecting_BackgroundPixels' Up to Down: nLenOfObjectBoundaryCurf = %d, sColor_Imagef->nLength - 1 = %d, iWidf = %d",
					nLenOfObjectBoundaryCurf, sColor_Imagef->nLength - 1, iWidf);

				printf("\n\n Please press any key to exit");
				getchar(); exit(1);
			}// if (nLenOfObjectBoundaryCurf < 0 || nLenOfObjectBoundaryCurf > sColor_Imagef->nLength - 1)

			 //start counting from the right for the left object
			nNumOfBlackPixelsInALineTotf = 0;

			//for (iLenf = sColor_Imagef->nLength - 1; iLenf >= nLenOfObjectBoundaryCurf; iLenf--)
			for (iLenf = 0; iLenf < sColor_Imagef->nLength; iLenf++)
			{
				nIndexOfPixelCurf = iLenf + (iWidf*sColor_Imagef->nLength); // nLenMax);

				nRedCurf = sColor_Imagef->nRed_Arr[nIndexOfPixelCurf];
				nGreenCurf = sColor_Imagef->nGreen_Arr[nIndexOfPixelCurf];
				nBlueCurf = sColor_Imagef->nBlue_Arr[nIndexOfPixelCurf];

				if (iWidf == nWidFor1stRowWithBlackPixelsf) //sColor_Imagef->nWidth - 1)
				{
					if (nRedCurf == sColor_Imagef->nIntensityOfBackground_Red && nGreenCurf == sColor_Imagef->nIntensityOfBackground_Green &&
						nBlueCurf == sColor_Imagef->nIntensityOfBackground_Blue)
					{
						nNumOfBlackPixelsInALineTotf += 1;
						sColor_Imagef->nIsAPixelBackground_Arr[nIndexOfPixelCurf] = 1; // the pixel belongs to the background
						continue;
					} // if (nRedCurf == sColor_Imagef->nIntensityOfBackground_Red && nGreenCurf == sColor_Imagef->nIntensityOfBackground_Green && ...

				}//if (iWidf == nWidFor1stRowWithBlackPixelsf)
				else // if (iWidf < sColor_Imagef->nWidth - 1)
				{
					//if (iLenf == sColor_Imagef->nLength - 1)
					if (iLenf == 0)
					{
						// No neighbor to the left or left below
						/////////////////////////////////////////////////////
						nIndexOfPixelNeighborToBelowf = iLenf + ((iWidf + 1)*sColor_Imagef->nLength); // a neighbor to the exactly below
						nIsAPixel_BackgroundOnef = sColor_Imagef->nIsAPixelBackground_Arr[nIndexOfPixelNeighborToBelowf];

						if (nIsAPixel_BackgroundOnef == 1)
						{
							if (nRedCurf == sColor_Imagef->nIntensityOfBackground_Red && nGreenCurf == sColor_Imagef->nIntensityOfBackground_Green &&
								nBlueCurf == sColor_Imagef->nIntensityOfBackground_Blue)
							{
								nNumOfBlackPixelsInALineTotf += 1;
								sColor_Imagef->nIsAPixelBackground_Arr[nIndexOfPixelCurf] = 1; // the pixel belongs to the background

							} // if (nRedCurf == sColor_Imagef->nIntensityOfBackground_Red && nGreenCurf == sColor_Imagef->nIntensityOfBackground_Green && ...
							continue;
						} //if (nIsAPixel_BackgroundOnef == 1)

						  /////////////////////////////////////////////////////

						nIndexOfPixelNeighborToLeftBelowf = iLenf + 1 + ((iWidf + 1)*sColor_Imagef->nLength); // a neighbor to the right below
						nIsAPixel_BackgroundOnef = sColor_Imagef->nIsAPixelBackground_Arr[nIndexOfPixelNeighborToLeftBelowf];

						if (nIsAPixel_BackgroundOnef == 1)
						{
							if (nRedCurf == sColor_Imagef->nIntensityOfBackground_Red && nGreenCurf == sColor_Imagef->nIntensityOfBackground_Green &&
								nBlueCurf == sColor_Imagef->nIntensityOfBackground_Blue)
							{
								nNumOfBlackPixelsInALineTotf += 1;
								sColor_Imagef->nIsAPixelBackground_Arr[nIndexOfPixelCurf] = 1; // the pixel belongs to the background

							} // if (nRedCurf == sColor_Imagef->nIntensityOfBackground_Red && nGreenCurf == sColor_Imagef->nIntensityOfBackground_Green && ...
							continue;
						} //if (nIsAPixel_BackgroundOnef == 1)

					} //if (iLenf == 0)
					  //////////////////////////////////////////////////////////
					  //if (iLenf < sColor_Imagef->nLength - 1)
					if (iLenf > 0)
					{
						nIndexOfPixelNeighborToRightf = iLenf - 1 + (iWidf*sColor_Imagef->nLength); // a neighbor to the left (not to the right)

						nIsAPixel_BackgroundOnef = sColor_Imagef->nIsAPixelBackground_Arr[nIndexOfPixelNeighborToRightf];

						if (nIsAPixel_BackgroundOnef == 1)
						{

							if (nRedCurf == sColor_Imagef->nIntensityOfBackground_Red && nGreenCurf == sColor_Imagef->nIntensityOfBackground_Green &&
								nBlueCurf == sColor_Imagef->nIntensityOfBackground_Blue)
							{
								nNumOfBlackPixelsInALineTotf += 1;
								sColor_Imagef->nIsAPixelBackground_Arr[nIndexOfPixelCurf] = 1; // the pixel belongs to the background

							} // if (nRedCurf == sColor_Imagef->nIntensityOfBackground_Red && nGreenCurf == sColor_Imagef->nIntensityOfBackground_Green && ...
							continue;
						} //if (nIsAPixel_BackgroundOnef == 1)
						  /////////////////////////////////////////////////////

						nIndexOfPixelNeighborToRightBelowf = iLenf + 1 + ((iWidf + 1)*sColor_Imagef->nLength); // a neighbor to the right below
						nIsAPixel_BackgroundOnef = sColor_Imagef->nIsAPixelBackground_Arr[nIndexOfPixelNeighborToRightBelowf];

						if (nIsAPixel_BackgroundOnef == 1)
						{

							if (nRedCurf == sColor_Imagef->nIntensityOfBackground_Red && nGreenCurf == sColor_Imagef->nIntensityOfBackground_Green &&
								nBlueCurf == sColor_Imagef->nIntensityOfBackground_Blue)
							{
								nNumOfBlackPixelsInALineTotf += 1;
								sColor_Imagef->nIsAPixelBackground_Arr[nIndexOfPixelCurf] = 1; // the pixel belongs to the background

							} // if (nRedCurf == sColor_Imagef->nIntensityOfBackground_Red && nGreenCurf == sColor_Imagef->nIntensityOfBackground_Green && ...
							continue;
						} //if (nIsAPixel_BackgroundOnef == 1)

						  /////////////////////////////////////////////////////

						nIndexOfPixelNeighborToBelowf = iLenf + ((iWidf + 1)*sColor_Imagef->nLength); // a neighbor to the exactly below
						nIsAPixel_BackgroundOnef = sColor_Imagef->nIsAPixelBackground_Arr[nIndexOfPixelNeighborToBelowf];

						if (nIsAPixel_BackgroundOnef == 1)
						{

							if (nRedCurf == sColor_Imagef->nIntensityOfBackground_Red && nGreenCurf == sColor_Imagef->nIntensityOfBackground_Green &&
								nBlueCurf == sColor_Imagef->nIntensityOfBackground_Blue)
							{
								nNumOfBlackPixelsInALineTotf += 1;
								sColor_Imagef->nIsAPixelBackground_Arr[nIndexOfPixelCurf] = 1; // the pixel belongs to the background

							} // if (nRedCurf == sColor_Imagef->nIntensityOfBackground_Red && nGreenCurf == sColor_Imagef->nIntensityOfBackground_Green && ...
							continue;
						} //if (nIsAPixel_BackgroundOnef == 1)

						  /////////////////////////////////////////////////////
						nIndexOfPixelNeighborToLeftBelowf = iLenf - 1 + ((iWidf + 1)*sColor_Imagef->nLength); // a neighbor to the left below
						nIsAPixel_BackgroundOnef = sColor_Imagef->nIsAPixelBackground_Arr[nIndexOfPixelNeighborToLeftBelowf];

						if (nIsAPixel_BackgroundOnef == 1)
						{
							if (nRedCurf == sColor_Imagef->nIntensityOfBackground_Red && nGreenCurf == sColor_Imagef->nIntensityOfBackground_Green &&
								nBlueCurf == sColor_Imagef->nIntensityOfBackground_Blue)
							{
								nNumOfBlackPixelsInALineTotf += 1;
								sColor_Imagef->nIsAPixelBackground_Arr[nIndexOfPixelCurf] = 1; // the pixel belongs to the background

							} // if (nRedCurf == sColor_Imagef->nIntensityOfBackground_Red && nGreenCurf == sColor_Imagef->nIntensityOfBackground_Green && ...
							continue;
						} //if (nIsAPixel_BackgroundOnef == 1)

					} //if (iLenf > 0)

				} // else // if (iWidf < sColor_Imagef->nWidth - 1)

			} //for (iLenf = 0; iLenf < sColor_Imagef->nLength; iLenf++)

			  //nNumOfBlackPixelsInTheLastRowMinf
			if (iWidf == nWidFor1stRowWithBlackPixelsf) //sColor_Imagef->nWidth - 1)
			{
				if (nNumOfBlackPixelsInALineTotf < nNumOfBlackPixelsInTheLastRowMinf)
				{
					nWidFor1stRowWithBlackPixelsf = nWidFor1stRowWithBlackPixelsf - 1;

					if (sColor_Imagef->nWidth - nWidFor1stRowWithBlackPixelsf > nNumOfTheLastRowsWithInsufficientNumberOfBlackPixelsMaxf)
					{
						printf("\n\n An error in 'Detecting_BackgroundPixels' from Up to Down 2: sColor_Imagef->nWidth - nWidFor1stRowWithBlackPixelsf = %d > nNumOfTheLastRowsWithInsufficientNumberOfBlackPixelsMaxf = %d",
							sColor_Imagef->nWidth - nWidFor1stRowWithBlackPixelsf, nNumOfTheLastRowsWithInsufficientNumberOfBlackPixelsMaxf);

						printf("\n nNumOfBlackPixelsInALineTotf = %d, nLenOfObjectBoundaryCurf = %d, sColor_Imagef->nLength - 1 = %d, iWidf = %d",
							nNumOfBlackPixelsInALineTotf, nLenOfObjectBoundaryCurf, sColor_Imagef->nLength - 1, iWidf);

						fprintf(fout, "\n\n An error in 'Detecting_BackgroundPixels' from Up to Down 2: sColor_Imagef->nWidth - nWidFor1stRowWithBlackPixelsf = %d > nNumOfTheLastRowsWithInsufficientNumberOfBlackPixelsMaxf = %d",
							sColor_Imagef->nWidth - nWidFor1stRowWithBlackPixelsf, nNumOfTheLastRowsWithInsufficientNumberOfBlackPixelsMaxf);

						fprintf(fout, "\n nNumOfBlackPixelsInALineTotf = %d, nLenOfObjectBoundaryCurf = %d, sColor_Imagef->nLength - 1 = %d, iWidf = %d",
							nNumOfBlackPixelsInALineTotf, nLenOfObjectBoundaryCurf, sColor_Imagef->nLength - 1, iWidf);

						printf("\n\n Please press any key to exit");
						getchar(); exit(1);

					} //if (sColor_Imagef->nWidth - nWidFor1stRowWithBlackPixelsf > nNumOfTheLastRowsWithInsufficientNumberOfBlackPixelsMaxf)

					continue; // for (iWidf = sColor_Imagef->nWidth - 1; iWidf >= 0; iWidf--)
				} //if (nNumOfBlackPixelsInALineTotf < nNumOfBlackPixelsInTheLastRowMinf) 

			}//if (iWidf == nWidFor1stRowWithBlackPixelsf)

			if (nNumOfBlackPixelsInALineTotf == 0)
			{

				printf("\n\n An error in 'Detecting_BackgroundPixels' from Up to Down 2: nNumOfBlackPixelsInALineTotf == 0, nLenOfObjectBoundaryCurf = %d, sColor_Imagef->nLength - 1 = %d, iWidf = %d",
					nLenOfObjectBoundaryCurf, sColor_Imagef->nLength - 1, iWidf);

				fprintf(fout, "\n\n An error in 'Detecting_BackgroundPixels' from Up to Down 2: nNumOfBlackPixelsInALineTotf == 0, nLenOfObjectBoundaryCurf = %d, sColor_Imagef->nLength - 1 = %d, iWidf = %d",
					nLenOfObjectBoundaryCurf, sColor_Imagef->nLength - 1, iWidf);

				printf("\n\n Please press any key to exit");
				getchar(); exit(1);
			} // if (nNumOfBlackPixelsInALineTotf == 0)

		} //for (iWidf = sColor_Imagef->nWidth - 1; iWidf >= 0; iWidf--)
		  //The end of Up to Down, the object is to the right

		  ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		  //from Down to Up, the object is to the right 
		  //for (iWidf = sColor_Imagef->nWidth - 1; iWidf >= 0; iWidf--)
		for (iWidf = 0; iWidf < sColor_Imagef->nWidth; iWidf++)
		{
			nLenOfObjectBoundaryCurf = sColor_Imagef->nLenObjectBoundary_Arr[iWidf];

			//start counting from the right for the left object
			nNumOfBlackPixelsInALineTotf = 0;

			//for (iLenf = sColor_Imagef->nLength - 1; iLenf >= nLenOfObjectBoundaryCurf; iLenf--)
			for (iLenf = 0; iLenf < sColor_Imagef->nLength; iLenf++)
			{
				nIndexOfPixelCurf = iLenf + (iWidf*sColor_Imagef->nLength); // nLenMax);

				if (sColor_Imagef->nIsAPixelBackground_Arr[nIndexOfPixelCurf] == 1)
				{
					nNumOfBlackPixelsInALineTotf += 1;
					continue;
				}// if (sColor_Imagef->nIsAPixelBackground_Arr[nIndexOfPixelCurf] == 1)

				nRedCurf = sColor_Imagef->nRed_Arr[nIndexOfPixelCurf];
				nGreenCurf = sColor_Imagef->nGreen_Arr[nIndexOfPixelCurf];
				nBlueCurf = sColor_Imagef->nBlue_Arr[nIndexOfPixelCurf];

				if (iWidf == 0)
				{
					if (nRedCurf == sColor_Imagef->nIntensityOfBackground_Red && nGreenCurf == sColor_Imagef->nIntensityOfBackground_Green &&
						nBlueCurf == sColor_Imagef->nIntensityOfBackground_Blue)
					{
						nNumOfBlackPixelsInALineTotf += 1;
						sColor_Imagef->nIsAPixelBackground_Arr[nIndexOfPixelCurf] = 1; // the pixel belongs to the background
						continue;
					} // if (nRedCurf == sColor_Imagef->nIntensityOfBackground_Red && nGreenCurf == sColor_Imagef->nIntensityOfBackground_Green && ...
				}//if (iWidf == 0)

				else // if (iWidf > 0)
				{

					//if (iLenf == sColor_Imagef->nLength - 1)
					if (iLenf == 0)
					{
						// No neighbor to the left or left Above

						/////////////////////////////////////////////////////
						nIndexOfPixelNeighborToAbovef = iLenf + ((iWidf - 1)*sColor_Imagef->nLength); // a neighbor to the exactly Above
						nIsAPixel_BackgroundOnef = sColor_Imagef->nIsAPixelBackground_Arr[nIndexOfPixelNeighborToAbovef];

						if (nIsAPixel_BackgroundOnef == 1)
						{

							if (nRedCurf == sColor_Imagef->nIntensityOfBackground_Red && nGreenCurf == sColor_Imagef->nIntensityOfBackground_Green &&
								nBlueCurf == sColor_Imagef->nIntensityOfBackground_Blue)
							{
								nNumOfBlackPixelsInALineTotf += 1;
								sColor_Imagef->nIsAPixelBackground_Arr[nIndexOfPixelCurf] = 1; // the pixel belongs to the background

							} // if (nRedCurf == sColor_Imagef->nIntensityOfBackground_Red && nGreenCurf == sColor_Imagef->nIntensityOfBackground_Green && ...
							continue;
						} //if (nIsAPixel_BackgroundOnef == 1)

						  /////////////////////////////////////////////////////

						nIndexOfPixelNeighborToLeftAbovef = iLenf + 1 + ((iWidf - 1)*sColor_Imagef->nLength); // a neighbor to the right Above
						nIsAPixel_BackgroundOnef = sColor_Imagef->nIsAPixelBackground_Arr[nIndexOfPixelNeighborToLeftAbovef];

						if (nIsAPixel_BackgroundOnef == 1)
						{

							if (nRedCurf == sColor_Imagef->nIntensityOfBackground_Red && nGreenCurf == sColor_Imagef->nIntensityOfBackground_Green &&
								nBlueCurf == sColor_Imagef->nIntensityOfBackground_Blue)
							{
								nNumOfBlackPixelsInALineTotf += 1;
								sColor_Imagef->nIsAPixelBackground_Arr[nIndexOfPixelCurf] = 1; // the pixel belongs to the background

							} // if (nRedCurf == sColor_Imagef->nIntensityOfBackground_Red && nGreenCurf == sColor_Imagef->nIntensityOfBackground_Green && ...
							continue;
						} //if (nIsAPixel_BackgroundOnef == 1)

					} //if (iLenf == 0)

					  //////////////////////////////////////////////////////////
					  //if (iLenf < sColor_Imagef->nLength - 1)
					if (iLenf > 0)
					{
						nIndexOfPixelNeighborToRightf = iLenf - 1 + (iWidf*sColor_Imagef->nLength); // a neighbor to the left (not to the right)

						nIsAPixel_BackgroundOnef = sColor_Imagef->nIsAPixelBackground_Arr[nIndexOfPixelNeighborToRightf];

						if (nIsAPixel_BackgroundOnef == 1)
						{

							if (nRedCurf == sColor_Imagef->nIntensityOfBackground_Red && nGreenCurf == sColor_Imagef->nIntensityOfBackground_Green &&
								nBlueCurf == sColor_Imagef->nIntensityOfBackground_Blue)
							{
								nNumOfBlackPixelsInALineTotf += 1;
								sColor_Imagef->nIsAPixelBackground_Arr[nIndexOfPixelCurf] = 1; // the pixel belongs to the background

							} // if (nRedCurf == sColor_Imagef->nIntensityOfBackground_Red && nGreenCurf == sColor_Imagef->nIntensityOfBackground_Green && ...
							continue;
						} //if (nIsAPixel_BackgroundOnef == 1)
						  /////////////////////////////////////////////////////

						nIndexOfPixelNeighborToRightAbovef = iLenf + 1 + ((iWidf - 1)*sColor_Imagef->nLength); // a neighbor to the right Above
						nIsAPixel_BackgroundOnef = sColor_Imagef->nIsAPixelBackground_Arr[nIndexOfPixelNeighborToRightAbovef];

						if (nIsAPixel_BackgroundOnef == 1)
						{

							if (nRedCurf == sColor_Imagef->nIntensityOfBackground_Red && nGreenCurf == sColor_Imagef->nIntensityOfBackground_Green &&
								nBlueCurf == sColor_Imagef->nIntensityOfBackground_Blue)
							{
								nNumOfBlackPixelsInALineTotf += 1;
								sColor_Imagef->nIsAPixelBackground_Arr[nIndexOfPixelCurf] = 1; // the pixel belongs to the background

							} // if (nRedCurf == sColor_Imagef->nIntensityOfBackground_Red && nGreenCurf == sColor_Imagef->nIntensityOfBackground_Green && ...
							continue;
						} //if (nIsAPixel_BackgroundOnef == 1)

						  /////////////////////////////////////////////////////

						nIndexOfPixelNeighborToAbovef = iLenf + ((iWidf - 1)*sColor_Imagef->nLength); // a neighbor to the exactly Above
						nIsAPixel_BackgroundOnef = sColor_Imagef->nIsAPixelBackground_Arr[nIndexOfPixelNeighborToAbovef];

						if (nIsAPixel_BackgroundOnef == 1)
						{

							if (nRedCurf == sColor_Imagef->nIntensityOfBackground_Red && nGreenCurf == sColor_Imagef->nIntensityOfBackground_Green &&
								nBlueCurf == sColor_Imagef->nIntensityOfBackground_Blue)
							{
								nNumOfBlackPixelsInALineTotf += 1;
								sColor_Imagef->nIsAPixelBackground_Arr[nIndexOfPixelCurf] = 1; // the pixel belongs to the background

							} // if (nRedCurf == sColor_Imagef->nIntensityOfBackground_Red && nGreenCurf == sColor_Imagef->nIntensityOfBackground_Green && ...
							continue;
						} //if (nIsAPixel_BackgroundOnef == 1)

						  /////////////////////////////////////////////////////
						nIndexOfPixelNeighborToLeftAbovef = iLenf - 1 + ((iWidf - 1)*sColor_Imagef->nLength); // a neighbor to the left Above
						nIsAPixel_BackgroundOnef = sColor_Imagef->nIsAPixelBackground_Arr[nIndexOfPixelNeighborToLeftAbovef];

						if (nIsAPixel_BackgroundOnef == 1)
						{
							if (nRedCurf == sColor_Imagef->nIntensityOfBackground_Red && nGreenCurf == sColor_Imagef->nIntensityOfBackground_Green &&
								nBlueCurf == sColor_Imagef->nIntensityOfBackground_Blue)
							{
								nNumOfBlackPixelsInALineTotf += 1;
								sColor_Imagef->nIsAPixelBackground_Arr[nIndexOfPixelCurf] = 1; // the pixel belongs to the background

							} // if (nRedCurf == sColor_Imagef->nIntensityOfBackground_Red && nGreenCurf == sColor_Imagef->nIntensityOfBackground_Green && ...
							continue;
						} //if (nIsAPixel_BackgroundOnef == 1)

					} //if (iLenf > 0)

				} // else // if (iWidf < sColor_Imagef->nWidth - 1)

			} //for (iLenf = 0; iLenf < sColor_Imagef->nLength; iLenf++)

		} //for (iWidf = 0; iWidf < sColor_Imagef->nWidth; iWidf++)
		  //The end of Down to Up, the object is to the right

	} // if (sColor_Imagef->nSideOfObjectLocation == 1)

	return 1;
} //int Detecting_BackgroundPixels(...

  /////////////////////////////////////////////////////////////////////
  //nIntensityToReplaceBlackObjectPixels
int FillingOut_BlackObjectPixels(

	COLOR_IMAGE *sColor_Imagef) //[nImageSizeMax]
{
	int
		nResf,
		nIndexOfPixelCurf,

		iWidf,
		iLenf,

		nRedCurf,
		nGreenCurf,
		nBlueCurf,

		nBottomOfBlackPixelsf = -1,
		nTopOfBlackPixelsf = -1,

		nNumOfBlackPixelsInALineTotf,
		nLenOfObjectBoundaryCurf;

	int *nLeftBoundaryOfBlackPixelsArrf = new int[sColor_Imagef->nWidth];

	int *nRightBoundaryOfBlackPixelsArrf = new int[sColor_Imagef->nWidth];

	if (nLeftBoundaryOfBlackPixelsArrf == nullptr || nRightBoundaryOfBlackPixelsArrf == nullptr)
	{
		printf("\n\n An error in 'FillingOut_BlackObjectPixels': nLeftBoundaryOfBlackPixelsArrf == nullptr || nRightBoundaryOfBlackPixelsArrf == nullptr");
		fprintf(fout, "\n\n An error in 'FillingOut_BlackObjectPixels': nLeftBoundaryOfBlackPixelsArrf == nullptr || nRightBoundaryOfBlackPixelsArrf == nullptr");

		printf("\n\n Please press any key to exit");
		getchar(); exit(1);
	} // if (nLeftBoundaryOfBlackPixelsArrf == nullptr || nRightBoundaryOfBlackPixelsArrf == nullptr)

	  //object to the left   
	if (sColor_Imagef->nSideOfObjectLocation == -1)
	{

		//finding the left and right boundaries of black object pixels 
		for (iWidf = 0; iWidf < sColor_Imagef->nWidth; iWidf++)
		{
			nLeftBoundaryOfBlackPixelsArrf[iWidf] = -1;
			nRightBoundaryOfBlackPixelsArrf[iWidf] = -1;

			nLenOfObjectBoundaryCurf = sColor_Imagef->nLenObjectBoundary_Arr[iWidf];
			if (nLenOfObjectBoundaryCurf < 0 || nLenOfObjectBoundaryCurf > sColor_Imagef->nLength - 1)
			{
				if (nLenOfObjectBoundaryCurf == -1)
				{
					fprintf(fout, "\n\n No boundary/erasing in 'FillingOut_BlackObjectPixels' 1: nLenOfObjectBoundaryCurf = %d, sColor_Imagef->nLength - 1 = %d, iWidf = %d",
						nLenOfObjectBoundaryCurf, sColor_Imagef->nLength - 1, iWidf);

					continue;
				} //if (nLenOfObjectBoundaryCurf == -1)

				fprintf(fout, "\n\n An error in 'FillingOut_BlackObjectPixels' 1: nLenOfObjectBoundaryCurf = %d, sColor_Imagef->nLength - 1 = %d, iWidf = %d",
					nLenOfObjectBoundaryCurf, sColor_Imagef->nLength - 1, iWidf);
				printf("\n\n An error in 'FillingOut_BlackObjectPixels' 1: nLenOfObjectBoundaryCurf = %d, sColor_Imagef->nLength - 1 = %d, iWidf = %d",
					nLenOfObjectBoundaryCurf, sColor_Imagef->nLength - 1, iWidf);

				printf("\n\n Please press any key to exit");
				getchar(); exit(1);
			}// if (nLenOfObjectBoundaryCurf < 0 || nLenOfObjectBoundaryCurf > sColor_Imagef->nLength - 1)

			 //start counting from the left
			nNumOfBlackPixelsInALineTotf = 0;
			for (iLenf = 0; iLenf < nLenOfObjectBoundaryCurf; iLenf++)
				//for (iLenf = sColor_Imagef->nLength - 1; iLenf > nLenOfObjectBoundaryCurf; iLenf--)
			{
				nIndexOfPixelCurf = iLenf + (iWidf*sColor_Imagef->nLength); // nLenMax);

				nRedCurf = sColor_Imagef->nRed_Arr[nIndexOfPixelCurf];
				nGreenCurf = sColor_Imagef->nGreen_Arr[nIndexOfPixelCurf];
				nBlueCurf = sColor_Imagef->nBlue_Arr[nIndexOfPixelCurf];

				//if (nRedCurf != sColor_Imagef->nIntensityOfBackground_Red)
				if (nRedCurf == sColor_Imagef->nIntensityOfBackground_Red && nGreenCurf == sColor_Imagef->nIntensityOfBackground_Green &&
					nBlueCurf == sColor_Imagef->nIntensityOfBackground_Blue)
				{

					if (sColor_Imagef->nIsAPixelBackground_Arr[nIndexOfPixelCurf] == 0)
					{
						sColor_Imagef->nIsAPixelBackground_Arr[nIndexOfPixelCurf] = 2; //was replaced
						sColor_Imagef->nRed_Arr[nIndexOfPixelCurf] = nIntensityToReplaceBlackObjectPixels;
						sColor_Imagef->nGreen_Arr[nIndexOfPixelCurf] = nIntensityToReplaceBlackObjectPixels;
						sColor_Imagef->nBlue_Arr[nIndexOfPixelCurf] = nIntensityToReplaceBlackObjectPixels;

						nNumOfBlackPixelsInALineTotf += 1;

						if (nBottomOfBlackPixelsf == -1)
						{
							nBottomOfBlackPixelsf = iWidf;
						} //if (nBottomOfBlackPixelsf == -1)

						if (nBottomOfBlackPixelsf != -1 && nTopOfBlackPixelsf != iWidf)
						{
							nTopOfBlackPixelsf = iWidf;
						} //if (nBottomOfBlackPixelsf != -1 && ..)

						  //sColor_Imagef->nRed_Arr[nIndexOfPixelCurf] = sColor_Imagef->nIntensityOfBackground_Red;
						if (nLeftBoundaryOfBlackPixelsArrf[iWidf] == -1)
						{
							nLeftBoundaryOfBlackPixelsArrf[iWidf] = iLenf;
						} //if (nLeftBoundaryOfBlackPixelsArrf[iWidf] == -1)

						if (nLeftBoundaryOfBlackPixelsArrf[iWidf] != -1)
						{
							nRightBoundaryOfBlackPixelsArrf[iWidf] = iLenf;
						} //if (nLeftBoundaryOfBlackPixelsArrf[iWidf] != -1)

					} // if (sColor_Imagef->nIsAPixelBackground_Arr[nIndexOfPixelCurf] == 0)

				} // if (nRedCurf == sColor_Imagef->nIntensityOfBackground_Red && nGreenCurf == sColor_Imagef->nIntensityOfBackground_Green && ...

			} //for (iLenf = 0; iLenf < nLenOfObjectBoundaryCurf; iLenf++)

		} //for (iWidf = 0; iWidf < sColor_Imagef->nWidth; iWidf++)
	} // if (sColor_Imagef->nSideOfObjectLocation == -1)

	  //object to the right
	if (sColor_Imagef->nSideOfObjectLocation == 1)
	{
		for (iWidf = 0; iWidf < sColor_Imagef->nWidth; iWidf++)
		{
			//start counting from the right

			nLenOfObjectBoundaryCurf = sColor_Imagef->nLenObjectBoundary_Arr[iWidf];
			if (nLenOfObjectBoundaryCurf < 0 || nLenOfObjectBoundaryCurf > sColor_Imagef->nLength - 1)
			{
				if (nLenOfObjectBoundaryCurf == -1)
				{
					fprintf(fout, "\n\n No boundary/erasing in 'FillingOut_BlackObjectPixels' 2: nLenOfObjectBoundaryCurf = %d, sColor_Imagef->nLength - 1 = %d, iWidf = %d",
						nLenOfObjectBoundaryCurf, sColor_Imagef->nLength - 1, iWidf);

					continue;
				} //if (nLenOfObjectBoundaryCurf == -1)

				fprintf(fout, "\n\n An error in 'FillingOut_BlackObjectPixels' 2: nLenOfObjectBoundaryCurf = %d, sColor_Imagef->nLength - 1 = %d, iWidf = %d",
					nLenOfObjectBoundaryCurf, sColor_Imagef->nLength - 1, iWidf);
				printf("\n\n An error in 'FillingOut_BlackObjectPixels' 2: nLenOfObjectBoundaryCurf = %d, sColor_Imagef->nLength - 1 = %d, iWidf = %d",
					nLenOfObjectBoundaryCurf, sColor_Imagef->nLength - 1, iWidf);

				printf("\n\n Please press any key to exit");
				getchar(); exit(1);
			}// if (nLenOfObjectBoundaryCurf < 0 || nLenOfObjectBoundaryCurf > sColor_Imagef->nLength - 1)

			nLeftBoundaryOfBlackPixelsArrf[iWidf] = -1;
			nRightBoundaryOfBlackPixelsArrf[iWidf] = -1;

			nLenOfObjectBoundaryCurf = sColor_Imagef->nLenObjectBoundary_Arr[iWidf];
			if (nLenOfObjectBoundaryCurf < 0 || nLenOfObjectBoundaryCurf > sColor_Imagef->nLength - 1)
			{
				if (nLenOfObjectBoundaryCurf == -1)
				{
					fprintf(fout, "\n\n No boundary/erasing in 'FillingOut_BlackObjectPixels' 1: nLenOfObjectBoundaryCurf = %d, sColor_Imagef->nLength - 1 = %d, iWidf = %d",
						nLenOfObjectBoundaryCurf, sColor_Imagef->nLength - 1, iWidf);

					continue;
				} //if (nLenOfObjectBoundaryCurf == -1)

				fprintf(fout, "\n\n An error in 'FillingOut_BlackObjectPixels' 1: nLenOfObjectBoundaryCurf = %d, sColor_Imagef->nLength - 1 = %d, iWidf = %d",
					nLenOfObjectBoundaryCurf, sColor_Imagef->nLength - 1, iWidf);
				printf("\n\n An error in 'FillingOut_BlackObjectPixels' 1: nLenOfObjectBoundaryCurf = %d, sColor_Imagef->nLength - 1 = %d, iWidf = %d",
					nLenOfObjectBoundaryCurf, sColor_Imagef->nLength - 1, iWidf);

				printf("\n\n Please press any key to exit");
				getchar(); exit(1);
			}// if (nLenOfObjectBoundaryCurf < 0 || nLenOfObjectBoundaryCurf > sColor_Imagef->nLength - 1)

			 //start counting from the left
			nNumOfBlackPixelsInALineTotf = 0;
			//for (iLenf = 0; iLenf < nLenOfObjectBoundaryCurf; iLenf++)
			for (iLenf = sColor_Imagef->nLength - 1; iLenf > nLenOfObjectBoundaryCurf; iLenf--)
			{
				nIndexOfPixelCurf = iLenf + (iWidf*sColor_Imagef->nLength); // nLenMax);

				nRedCurf = sColor_Imagef->nRed_Arr[nIndexOfPixelCurf];
				nGreenCurf = sColor_Imagef->nGreen_Arr[nIndexOfPixelCurf];
				nBlueCurf = sColor_Imagef->nBlue_Arr[nIndexOfPixelCurf];

				//if (nRedCurf != sColor_Imagef->nIntensityOfBackground_Red)
				if (nRedCurf == sColor_Imagef->nIntensityOfBackground_Red && nGreenCurf == sColor_Imagef->nIntensityOfBackground_Green &&
					nBlueCurf == sColor_Imagef->nIntensityOfBackground_Blue)
				{

					if (sColor_Imagef->nIsAPixelBackground_Arr[nIndexOfPixelCurf] == 0)
					{
						sColor_Imagef->nIsAPixelBackground_Arr[nIndexOfPixelCurf] = 2; //was replaced
						sColor_Imagef->nRed_Arr[nIndexOfPixelCurf] = nIntensityToReplaceBlackObjectPixels;
						sColor_Imagef->nGreen_Arr[nIndexOfPixelCurf] = nIntensityToReplaceBlackObjectPixels;
						sColor_Imagef->nBlue_Arr[nIndexOfPixelCurf] = nIntensityToReplaceBlackObjectPixels;

						nNumOfBlackPixelsInALineTotf += 1;

						if (nBottomOfBlackPixelsf == -1)
						{
							nBottomOfBlackPixelsf = iWidf;
						} //if (nBottomOfBlackPixelsf == -1)

						if (nBottomOfBlackPixelsf != -1 && nTopOfBlackPixelsf != iWidf)
						{
							nTopOfBlackPixelsf = iWidf;
						} //if (nBottomOfBlackPixelsf != -1 && ..)

						  //sColor_Imagef->nRed_Arr[nIndexOfPixelCurf] = sColor_Imagef->nIntensityOfBackground_Red;
						if (nLeftBoundaryOfBlackPixelsArrf[iWidf] == -1)
						{
							nLeftBoundaryOfBlackPixelsArrf[iWidf] = iLenf;
						} //if (nLeftBoundaryOfBlackPixelsArrf[iWidf] == -1)

						if (nLeftBoundaryOfBlackPixelsArrf[iWidf] != -1)
						{
							nRightBoundaryOfBlackPixelsArrf[iWidf] = iLenf;
						} //if (nLeftBoundaryOfBlackPixelsArrf[iWidf] != -1)

					} // if (sColor_Imagef->nIsAPixelBackground_Arr[nIndexOfPixelCurf] == 0)

				} // if (nRedCurf == sColor_Imagef->nIntensityOfBackground_Red && nGreenCurf == sColor_Imagef->nIntensityOfBackground_Green && ...

			} //for (iLenf = sColor_Imagef->nLength - 1; iLenf > nLenOfObjectBoundaryCurf; iLenf--)

		} //for (iWidf = 0; iWidf < sColor_Imagef->nWidth; iWidf++)
	} // if (sColor_Imagef->nSideOfObjectLocation == 1)

	delete[] nLeftBoundaryOfBlackPixelsArrf;
	delete[] nRightBoundaryOfBlackPixelsArrf;

	return 1;
} //int FillingOut_BlackObjectPixels(...
///////////////////////////////////////////////////////////////////

int Initializing_Color_To_CurSize(
	const int nImageWidthf,
	const int nImageLengthf,

	COLOR_IMAGE *sColor_Imagef) //[]
{
	int
		nIndexOfPixelCurf,

		nImageSizeCurf = nImageWidthf*nImageLengthf,
		iWidf,
		iLenf;

	sColor_Imagef->nSideOfObjectLocation = 0; // neither left nor right
	sColor_Imagef->nIntensityOfBackground_Red = -1;
	sColor_Imagef->nIntensityOfBackground_Green = -1;
	sColor_Imagef->nIntensityOfBackground_Blue = -1;

	sColor_Imagef->nWidth = nImageWidthf;
	sColor_Imagef->nLength = nImageLengthf;

	//sGrayscale_Imagef->nPixel_ValidOrNotArr = new int[nImageSizeCurf];

	sColor_Imagef->nLenObjectBoundary_Arr = new int[nImageWidthf];

	sColor_Imagef->nRed_Arr = new int[nImageSizeCurf];
	sColor_Imagef->nGreen_Arr = new int[nImageSizeCurf];
	sColor_Imagef->nBlue_Arr = new int[nImageSizeCurf];

	
	sColor_Imagef->nIsAPixelBackground_Arr = new int[nImageSizeCurf];

	for (iWidf = 0; iWidf < nImageWidthf; iWidf++)
	{
		sColor_Imagef->nLenObjectBoundary_Arr[iWidf] = -1;

		for (iLenf = 0; iLenf < nImageLengthf; iLenf++)
		{
			nIndexOfPixelCurf = iLenf + (iWidf*nImageLengthf);

			//sGrayscale_Imagef->nPixel_ValidOrNotArr[nIndexOfPixelCurf] = 1; //valid for no restrictions
			sColor_Imagef->nRed_Arr[nIndexOfPixelCurf] = -1;
			sColor_Imagef->nGreen_Arr[nIndexOfPixelCurf] = -1;
			sColor_Imagef->nBlue_Arr[nIndexOfPixelCurf] = -1;

			sColor_Imagef->nIsAPixelBackground_Arr[nIndexOfPixelCurf] = 0; //all pixels belong to the object
		} //for (iLenf = 0; iLenf < nImageLengthf; iLenf++)
	} // for (iWidf = 0; iWidf < nImageWidthf; iWidf++)

	return 1;
} //int Initializing_Color_To_CurSize(...



//printf("\n\n Please press any key:"); getchar();