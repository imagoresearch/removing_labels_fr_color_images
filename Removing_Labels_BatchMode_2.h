#define _CRT_SECURE_NO_DEPRECATE // 1
#define _CRT_NONSTDC_NO_DEPRECATE // 1

//using namespace imago;

#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <ctype.h>
#include <time.h>
//#include <iostream.h>
#include <string.h>
//#include <iomanip.h>
//#include <fstream.h>
#include <assert.h>

//#include <dirent.h>
	#include "image.h"
#include <dirent.h>

#define MASK_OUTPUT

#define SQRT2 1.414213562
#define BLACK 0
#define WHITE 1

#define nLenMax 6000 
#define nWidMax 6000 

#define nLenMin 50 
#define nWidMin 50

#define nImageSizeMax (nLenMax*nWidMax)

///////////////////////////////////////////////////////////////////
#define nLarge 1000000
#define fLarge 1.0E+10 //6

#define feps 0.000001
//#define pi 3.14159
#define PI 3.1415926535

//////////////////////////////////////////////////////////////////
#define nLenSubimageToPrintMin 0 //342
#define nLenSubimageToPrintMax 4000  //2500 //1800 //426

#define nWidSubimageToPrintMin 191 //344 //193 //416 //814
#define nWidSubimageToPrintMax 192 //361 //195 //240 //420
///////////////////////////////////////////////////////////////////////

#define nIntensityStatMin 10 //85
#define nIntensityStatMinForEqualization 0 //85

#define nIntensityStatForReadMax 65535

#define nIntensityStatMax 255

//#define nNumOfHistogramBinsStat (nIntensityStatMax) //(nIntensityStatMax - nIntensityStatMin + 1)
#define nNumOfHistogramBinsStat (nIntensityStatMax + 1) //(nIntensityStatMax - nIntensityStatMin + 1)

#define nIntensityThresholdForDense 150  // > nIntensityStatMin) and < nIntensityStatMax

#define nIntensityToReplaceBlackObjectPixels 20 // > nIntensityCloseToBlackMax
#define fLowestPercentageOfBlackPixelsInTheLastRow 0.4

#define fLargestPercentageOfTheLastRowsWithInsufficientNumberOfBlackPixels 0.1

//////////////////////////////////////////////////////////////////


#define nLengthOfTheSamePixelIntensitiesMin 100

#define nLengthOfTheSamePixelIntensities_ForObjectBoundary_Min 4 //40 //20 //10 //5 //3
//#define nLengthOfTheSamePixelIntensities_ForObjectBoundary_Min 90 //350 //250 //200 //150

#define nNumOfBoundariesForOneWid_Max 2
#define nLenOfALabel_Max 400

#define fCoefDistOf1stObjectPixel_ToImageEdgeMax 0.55 //0.6 //0.7

#define nWidDiffFor1stObjectPixelVerification 110 
#define nNumOfAddedWidMax 6 //15 //10 //6 //2

#define nLenDiffFor1stObjectPixelVerification 250 


//#define nDistBetweenBoundariesOfNeighborWids_Max 150 //90 //70 //50
#define nDistBetweenBoundariesOfNeighborWids_Max 200 //

#define fShareOfWidthForTheSamePixelIntensities_1 0.5
#define fShareOfWidthForTheSamePixelIntensities_2 0.25

//#define nLenOfNeighboringPixelsFrTheSameRow 70 
#define nLenOfNeighboringPixelsFrTheSameRow 40 //30 //20
#define nNumOfBlackNeighboingPixelsFrTheSameRowMax 4 //5 //<=nLenOfNeighboringPixelsFrTheSameRow

#define nIntensityCloseToBlackMax 9 //8 //3 //4 // <= nAverIntensityOverNeighboingPixelsFrTheSameRowMin, > sColor_Imagef->nIntensityOfBackground_Red, etc.

#define nAverIntensityOverNeighboingPixelsFrTheSameRowMin 10 //12 // > nIntensityCloseToBlackMax


/////////////////////////////////////////////////////////

typedef struct
{
	int
		nRed;
	
	int
		nGreen;
	int
		nBlue;

} WEIGHTED_RGB_COLORS;

typedef struct
{
	int nSideOfObjectLocation; //-1 - left, 1 - right

	int nIntensityOfBackground_Red; //
	int nIntensityOfBackground_Green; //
	int nIntensityOfBackground_Blue; //

	int nWidth;
	int nLength;

	int *nLenObjectBoundary_Arr;

//	int nRed_Arr[nImageSizeMax];
//	int nGreen_Arr[nImageSizeMax];
//	int nBlue_Arr[nImageSizeMax];
	int *nRed_Arr;
	int *nGreen_Arr;
	int *nBlue_Arr;

	int *nIsAPixelBackground_Arr;

} COLOR_IMAGE;

/*
typedef struct
{
	int nWidth;
	int nLength;

	int *nPixel_ValidOrNotArr; //1 - valid, 0 - invalid
	int *nGrayScale_Arr;
	
} GRAYSCALE_IMAGE;
*/

/*
typedef struct
{
	int nRedMinf; // = 140; //0 //10
	int nRedMaxf; // = 180; //

					   //#define nGreenMin 0 //0 //10
					   //#define nGreenMax 255 //
	int nGreenMinf; // = 10; //0 //10
	int nGreenMaxf; // = 60; //

						//#define nBlueMin 0 //0 //10
						//#define nBlueMax 255 //
	int nBlueMinf; // = 30; //0 //10
	int nBlueMaxf; // = 80; //


} PARAMETERS_REMOVAL_OF_LABELS;
*/
